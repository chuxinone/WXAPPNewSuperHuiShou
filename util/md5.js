var n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(n) {
    return typeof n;
} : function(n) {
    return n && "function" == typeof Symbol && n.constructor === Symbol && n !== Symbol.prototype ? "symbol" : typeof n;
}, t = "function" == typeof Symbol && "symbol" == n(Symbol.iterator) ? function(t) {
    return void 0 === t ? "undefined" : n(t);
} : function(t) {
    return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : void 0 === t ? "undefined" : n(t);
};

!function(n) {
    function o(n, t) {
        var o = (65535 & n) + (65535 & t);
        return (n >> 16) + (t >> 16) + (o >> 16) << 16 | 65535 & o;
    }
    function r(n, t) {
        return n << t | n >>> 32 - t;
    }
    function e(n, t, e, u, f, c) {
        return o(r(o(o(t, n), o(u, c)), f), e);
    }
    function u(n, t, o, r, u, f, c) {
        return e(t & o | ~t & r, n, t, u, f, c);
    }
    function f(n, t, o, r, u, f, c) {
        return e(t & r | o & ~r, n, t, u, f, c);
    }
    function c(n, t, o, r, u, f, c) {
        return e(t ^ o ^ r, n, t, u, f, c);
    }
    function i(n, t, o, r, u, f, c) {
        return e(o ^ (t | ~r), n, t, u, f, c);
    }
    function d(n, t) {
        n[t >> 5] |= 128 << t % 32, n[14 + (t + 64 >>> 9 << 4)] = t;
        var r, e, d, l, y, m = 1732584193, a = -271733879, b = -1732584194, p = 271733878;
        for (r = 0; r < n.length; r += 16) e = m, d = a, l = b, y = p, a = i(a = i(a = i(a = i(a = c(a = c(a = c(a = c(a = f(a = f(a = f(a = f(a = u(a = u(a = u(a = u(a, b = u(b, p = u(p, m = u(m, a, b, p, n[r], 7, -680876936), a, b, n[r + 1], 12, -389564586), m, a, n[r + 2], 17, 606105819), p, m, n[r + 3], 22, -1044525330), b = u(b, p = u(p, m = u(m, a, b, p, n[r + 4], 7, -176418897), a, b, n[r + 5], 12, 1200080426), m, a, n[r + 6], 17, -1473231341), p, m, n[r + 7], 22, -45705983), b = u(b, p = u(p, m = u(m, a, b, p, n[r + 8], 7, 1770035416), a, b, n[r + 9], 12, -1958414417), m, a, n[r + 10], 17, -42063), p, m, n[r + 11], 22, -1990404162), b = u(b, p = u(p, m = u(m, a, b, p, n[r + 12], 7, 1804603682), a, b, n[r + 13], 12, -40341101), m, a, n[r + 14], 17, -1502002290), p, m, n[r + 15], 22, 1236535329), b = f(b, p = f(p, m = f(m, a, b, p, n[r + 1], 5, -165796510), a, b, n[r + 6], 9, -1069501632), m, a, n[r + 11], 14, 643717713), p, m, n[r], 20, -373897302), b = f(b, p = f(p, m = f(m, a, b, p, n[r + 5], 5, -701558691), a, b, n[r + 10], 9, 38016083), m, a, n[r + 15], 14, -660478335), p, m, n[r + 4], 20, -405537848), b = f(b, p = f(p, m = f(m, a, b, p, n[r + 9], 5, 568446438), a, b, n[r + 14], 9, -1019803690), m, a, n[r + 3], 14, -187363961), p, m, n[r + 8], 20, 1163531501), b = f(b, p = f(p, m = f(m, a, b, p, n[r + 13], 5, -1444681467), a, b, n[r + 2], 9, -51403784), m, a, n[r + 7], 14, 1735328473), p, m, n[r + 12], 20, -1926607734), b = c(b, p = c(p, m = c(m, a, b, p, n[r + 5], 4, -378558), a, b, n[r + 8], 11, -2022574463), m, a, n[r + 11], 16, 1839030562), p, m, n[r + 14], 23, -35309556), b = c(b, p = c(p, m = c(m, a, b, p, n[r + 1], 4, -1530992060), a, b, n[r + 4], 11, 1272893353), m, a, n[r + 7], 16, -155497632), p, m, n[r + 10], 23, -1094730640), b = c(b, p = c(p, m = c(m, a, b, p, n[r + 13], 4, 681279174), a, b, n[r], 11, -358537222), m, a, n[r + 3], 16, -722521979), p, m, n[r + 6], 23, 76029189), b = c(b, p = c(p, m = c(m, a, b, p, n[r + 9], 4, -640364487), a, b, n[r + 12], 11, -421815835), m, a, n[r + 15], 16, 530742520), p, m, n[r + 2], 23, -995338651), b = i(b, p = i(p, m = i(m, a, b, p, n[r], 6, -198630844), a, b, n[r + 7], 10, 1126891415), m, a, n[r + 14], 15, -1416354905), p, m, n[r + 5], 21, -57434055), b = i(b, p = i(p, m = i(m, a, b, p, n[r + 12], 6, 1700485571), a, b, n[r + 3], 10, -1894986606), m, a, n[r + 10], 15, -1051523), p, m, n[r + 1], 21, -2054922799), b = i(b, p = i(p, m = i(m, a, b, p, n[r + 8], 6, 1873313359), a, b, n[r + 15], 10, -30611744), m, a, n[r + 6], 15, -1560198380), p, m, n[r + 13], 21, 1309151649), b = i(b, p = i(p, m = i(m, a, b, p, n[r + 4], 6, -145523070), a, b, n[r + 11], 10, -1120210379), m, a, n[r + 2], 15, 718787259), p, m, n[r + 9], 21, -343485551), 
        m = o(m, e), a = o(a, d), b = o(b, l), p = o(p, y);
        return [ m, a, b, p ];
    }
    function l(n) {
        var t, o = "", r = 32 * n.length;
        for (t = 0; t < r; t += 8) o += String.fromCharCode(n[t >> 5] >>> t % 32 & 255);
        return o;
    }
    function y(n) {
        var t, o = [];
        for (o[(n.length >> 2) - 1] = void 0, t = 0; t < o.length; t += 1) o[t] = 0;
        var r = 8 * n.length;
        for (t = 0; t < r; t += 8) o[t >> 5] |= (255 & n.charCodeAt(t / 8)) << t % 32;
        return o;
    }
    function m(n) {
        return l(d(y(n), 8 * n.length));
    }
    function a(n, t) {
        var o, r, e = y(n), u = [], f = [];
        for (u[15] = f[15] = void 0, e.length > 16 && (e = d(e, 8 * n.length)), o = 0; o < 16; o += 1) u[o] = 909522486 ^ e[o], 
        f[o] = 1549556828 ^ e[o];
        return r = d(u.concat(y(t)), 512 + 8 * t.length), l(d(f.concat(r), 640));
    }
    function b(n) {
        var t, o, r = "";
        for (o = 0; o < n.length; o += 1) t = n.charCodeAt(o), r += "0123456789abcdef".charAt(t >>> 4 & 15) + "0123456789abcdef".charAt(15 & t);
        return r;
    }
    function p(n) {
        return unescape(encodeURIComponent(n));
    }
    function h(n) {
        return m(p(n));
    }
    function v(n) {
        return b(h(n));
    }
    function g(n, t) {
        return a(p(n), p(t));
    }
    function s(n, t) {
        return b(g(n, t));
    }
    function S(n, t, o) {
        return t ? o ? g(t, n) : s(t, n) : o ? h(n) : v(n);
    }
    "function" == typeof define && define.amd ? define(function() {
        return S;
    }) : "object" == ("undefined" == typeof module ? "undefined" : t(module)) && module.exports ? module.exports = S : (void 0).md5 = S;
}();