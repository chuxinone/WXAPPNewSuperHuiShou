function e(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}

var t = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
    return typeof e;
} : function(e) {
    return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
};

Object.defineProperty(exports, "__esModule", {
    value: !0
});

var n = "function" == typeof Symbol && "symbol" == t(Symbol.iterator) ? function(e) {
    return void 0 === e ? "undefined" : t(e);
} : function(e) {
    return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : void 0 === e ? "undefined" : t(e);
}, r = e(require("./md5.js")), o = (e(require("./qqmap-wx-jssdk.min.js")), e(require("./amap-wx.js"))), a = require("../config/index");

exports.default = {
    post: function(e) {
        return e.method = "POST", e.header ? e.header["Content-Type"] = "application/x-www-form-urlencoded" : e.header = {
            "Content-Type": "application/x-www-form-urlencoded"
        }, wx.request(e);
    },
    get: function(e) {
        return e.url, wx.request(e);
    },
    filedFormat: function(e) {
        var t = "";
        for (var n in e) e.hasOwnProperty(n) && e[n] && (t += n + "=" + e[n] + "&");
        return t.length ? t.substr(0, t.length - 1) : t;
    },
    throttle: function(e, t) {
        var n = null;
        return function() {
            var r = this, o = arguments;
            clearTimeout(n), n = setTimeout(function() {
                e.apply(r, o);
            }, t);
        };
    },
    debouce: function(e, t, n) {
        var r = null;
        return function() {
            var o = this, a = arguments;
            if (r && clearTimeout(time), n) {
                var u = !r;
                r = setTimeout(function() {
                    r = null;
                }, t), u && e.apply(o, a);
            } else r = setTimeout(function() {
                e.apply(o, a);
            }, t);
        };
    },
    inArray: function(e, t, n) {
        if (e instanceof Array) {
            for (var r = !1, o = 0; o < e.length; o++) if (n) {
                if (e[o][n] == t[n]) {
                    r = !0;
                    break;
                }
            } else if (e[o] == t) {
                r = !0;
                break;
            }
            return r;
        }
    },
    setWhiteNavBar: function() {
        wx.setNavigationBarColor && wx.setNavigationBarColor({
            frontColor: "#000000",
            backgroundColor: "#ffffff"
        });
    },
    unique: function(e) {
        for (var t = [], n = 0; n < e.length; n++) -1 == t.indexOf(e[n]) && t.push(e[n]);
        return t;
    },
    formatNum: function(e) {
        return e > 9 ? e : "0" + e;
    },
    formatDate: function(e) {
        var t = (e = parseInt(e)) ? new Date(e) : new Date(), n = t.getFullYear(), r = t.getMonth() + 1, o = t.getDate(), a = t.getHours(), u = t.getMinutes(), i = t.getSeconds();
        return {
            Y: n,
            M: this.formatNum(r),
            D: this.formatNum(o),
            h: this.formatNum(a),
            m: this.formatNum(u),
            s: this.formatNum(i)
        };
    },
    getCurPageOpt: function() {
        var e = getCurrentPages();
        return e[e.length - 1].options;
    },
    sortByKey: function(e, t) {
        e.sort(function(e, n) {
            return n[t] - e[t];
        });
    },
    getAvailableCoupon: function(e, t) {
        t = parseInt(t);
        for (var n = [], r = 0; r < e.length; r++) e[r].useLimited = parseInt(e[r].useLimited), 
        e[r.faceValue] = parseInt(e[r.faceValue]), 10 == e[r].status && t >= e[r].useLimited / 100 && n.push(e[r]);
        return this.sortByKey(n, "faceValue"), n[0];
    },
    isMobile: function(e) {
        return /^1\d{10}$/.test(e);
    },
    isCardid: function(e) {
        return /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(e);
    },
    curTimeStamp: function() {
        return new Date().getTime();
    },
    qs: function(e) {
        if (0 === Object.keys(e).length) return "";
        var t = [];
        for (var n in e) e.hasOwnProperty(n) && t.push(n + "=" + e[n]);
        return t.join("&");
    },
    createAppParams: function(e, t) {
        var o = {
            head: {
                interface: e,
                version: "0.01",
                msgtype: "request"
            },
            params: {
                system: "CUSTAPP",
                time: parseInt(+new Date() / 1e3).toString()
            }
        };
        console.log(o.params.time), o.params = Object.assign({}, o.params, t);
        var a = Object.assign({}, o.head, o.params), u = Object.keys(a).map(function(e) {
            return "" === a[e] || "object" === n(a[e]) ? "" : e + "=" + a[e];
        }).filter(function(e) {
            return e;
        }).sort(function(e, t) {
            return e.charCodeAt(0) - t.charCodeAt(0);
        }).join("&"), i = (0, r.default)(u + "&key=m2cjgx46md5973n4ymeoxa4v195iwwmb").toLowerCase();
        return console.log(i), o.params.sign = i, o;
    },
    createBoxParams: function(e, t) {
        var n = {
            _head: {
                _interface: e,
                _version: "1.0",
                _msgtype: "request",
                _time: parseInt(+new Date() / 1e3).toString(),
                _remark: ""
            },
            _param: {}
        };
        return n._param = Object.assign({}, n._param, t), n;
    },
    getLocation: function() {
        return new Promise(function(e, t) {
            var n = new o.default.AMapWX({
                key: a.gdKey
            });
            wx.getLocation({
                success: function(r) {
                    console.log(r), r.latitude, r.longitude, n.getRegeo({
                        success: function(n) {
                            console.log(n), n ? e(n) : t(n);
                        }
                    });
                },
                fail: function(t) {
                    e(t);
                }
            });
        });
    }
};