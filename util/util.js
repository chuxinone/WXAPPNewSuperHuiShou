var t = function(t) {
    return t && t.__esModule ? t : {
        default: t
    };
}(require("bluebird"));

module.exports = {
    formatTime: function(t) {
        if ("number" != typeof t || t < 0) return t;
        var e = parseInt(t / 3600);
        return t %= 3600, [ e, parseInt(t / 60), t %= 60 ].map(function(t) {
            return (t = t.toString())[1] ? t : "0" + t;
        }).join(":");
    },
    Promise: function(e, r) {
        return r = r || {}, new t.default(function(t, u) {
            "function" != typeof e && u(), r.success = t, r.fail = u, e(r);
        });
    },
    Gesture: {
        touchstart: function(t, e) {
            var r = t.touches[0], u = r.clientX, a = r.clientY;
            e.setData({
                "gesture.startX": u,
                "gesture.startY": a,
                "gesture.out": !0
            });
        },
        isLeftSlide: function(t, e) {
            if (e.data.gesture.out) {
                var r = t.touches[0], u = r.clientX - e.data.gesture.startX, a = r.clientY - e.data.gesture.startY;
                return u < -20 && u > -40 && a < 8 && a > -8 && (e.setData({
                    "gesture.out": !1
                }), !0);
            }
        },
        isRightSlide: function(t, e) {
            if (e.data.gesture.out) {
                var r = t.touches[0], u = r.clientX - e.data.gesture.startX, a = r.clientY - e.data.gesture.startY;
                return u > 20 && u < 40 && a < 8 && a > -8 && (e.setData({
                    "gesture.out": !1
                }), !0);
            }
        }
    }
};