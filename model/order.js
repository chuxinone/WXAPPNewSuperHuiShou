Object.defineProperty(exports, "__esModule", {
    value: !0
});

var e = function(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}(require("../util/utils")), r = require("../config/index");

exports.default = {
    orders: function(t) {
        return new Promise(function(d, n) {
            e.default.post({
                url: r.url.orders,
                data: {
                    uid: t.uid,
                    openid: t.openid,
                    pageSize: t.pageSize || 6,
                    pageIndex: t.pageIndex
                },
                success: function(e) {
                    e = e.data, 0 === parseInt(e.errcode) ? d(e.data) : n(e.errmsg);
                }
            });
        });
    },
    cancelOrder: function(t) {
        var d = t.orderid, n = t.uid, i = t.openid;
        return new Promise(function(t, o) {
            e.default.post({
                url: r.url.cancelOrder,
                data: {
                    uid: n,
                    openid: i,
                    orderid: d
                },
                success: function(e) {
                    e = e.data, 0 === parseInt(e.errcode) ? t(e.data) : o(e.errmsg);
                }
            });
        });
    },
    order: function(t) {
        var d = t.orderid, n = t.uid, i = t.openid;
        return new Promise(function(t, o) {
            e.default.post({
                url: r.url.order,
                data: {
                    uid: n,
                    openid: i,
                    orderid: d
                },
                success: function(e) {
                    console.log(e), e = e.data, 0 === parseInt(e.errcode) ? t(e.data) : o(e.errmsg);
                }
            });
        });
    },
    take: function(t) {
        return new Promise(function(d, n) {
            e.default.post({
                url: r.url.takeOrder,
                data: {
                    tel: t.tel,
                    openid: t.openid,
                    selects: t.selects,
                    itemid: t.itemid,
                    ordertype: t.ordertype || "post",
                    visittime: t.visitTime,
                    address: t.address,
                    nickname: t.nickname,
                    price: t.price,
                    productname: t.productName
                },
                success: function(e) {
                    console.log("order:", e), e = e.data, 0 === parseInt(e.errcode) ? d(e.data) : n(e.errmsg);
                }
            });
        });
    },
    takeFcOrder: function(t) {
        return new Promise(function(d, n) {
            e.default.post({
                url: r.url.takeFcOrder,
                data: {
                    tel: t.tel || "",
                    orderid: t.orderid || "",
                    nickname: t.nickname || "",
                    province: t.province || "",
                    city: t.city || "",
                    county: t.county || "",
                    addr: t.addr || "",
                    cardid: t.cardid || ""
                },
                success: function(e) {
                    console.log(e), e = e.data, 0 === parseInt(e.errcode) ? d(e.data) : n(e.errmsg);
                }
            });
        });
    },
    takeSfOrder: function(t) {
        var d = t.orderid, n = t.nickname, i = t.tel, o = t.province, a = t.city, c = t.county, u = t.addr, s = t.sendtime;
        return new Promise(function(t, l) {
            e.default.post({
                url: r.url.takeSfOrder,
                data: {
                    tel: i,
                    orderid: d,
                    nickname: n,
                    province: o,
                    city: a,
                    county: c,
                    addr: u,
                    sendtime: s
                },
                success: function(e) {
                    e = e.data, 0 === parseInt(e.errcode) ? t(e.data) : l(e.errmsg);
                }
            });
        });
    }
};