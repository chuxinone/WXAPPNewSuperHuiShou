Object.defineProperty(exports, "__esModule", {
    value: !0
});

var e = require("../config/index"), r = function(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}(require("../util/utils"));

exports.default = {
    couponList: [],
    page: function(t) {
        var u = t.userId, o = t.pageIndex, n = t.pageSize, a = t.status, s = this;
        return new Promise(function(t, c) {
            r.default.get({
                url: e.url.coupon,
                data: {
                    platform: 2,
                    userId: u || 0,
                    pageIndex: o || 0,
                    pageSize: n || 1e3,
                    status: a || 1
                },
                success: function(e) {
                    0 == (e = e.data)._errCode ? (s.couponList = e._data.couponUseLogList, t(e._data)) : c(e._errStr);
                }
            });
        });
    },
    canUse: function(t) {
        var u = t.userId, o = t.proId, n = t.price, a = t.recycleType;
        return new Promise(function(t, s) {
            var c = [ {
                proId: o || 0,
                price: n || 0,
                num: 1
            } ];
            r.default.get({
                url: e.url.canUseCoupon,
                data: {
                    platform: 2,
                    userId: u || 0,
                    productList: c,
                    recycleType: a || 1
                },
                success: function(e) {
                    0 == (e = e.data)._errCode ? t(e._data) : s(e._errStr);
                }
            });
        });
    },
    gain: function(t) {
        var u = t.userId, o = t.id, n = t.isCouponParcel;
        return new Promise(function(t, a) {
            r.default.get({
                url: e.url.getCoupon,
                data: {
                    platform: 2,
                    userId: u || 0,
                    id: o || 0,
                    isCouponParcel: n || 1
                },
                success: function(e) {
                    e = e.data, console.log(e), 0 == e._errCode ? t(e._data) : a(e._errStr);
                },
                fail: function(e) {
                    console.log(e);
                }
            });
        });
    }
};