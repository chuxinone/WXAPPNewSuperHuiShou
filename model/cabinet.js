Object.defineProperty(exports, "__esModule", {
    value: !0
});

var e = require("../config/index"), t = function(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}(require("../util/utils"));

exports.default = {
    getBoxInfo: function(o, n, a) {
        return new Promise(function(u, i) {
            var r = t.default.createBoxParams("fengChao.getBoxInfo", {
                longitude: o,
                latitude: n,
                distanceRange: a
            });
            wx.request({
                url: e.url.getBoxInfo,
                header: {
                    "Content-Type": "application/json"
                },
                method: "post",
                data: JSON.stringify(r),
                success: function(e) {
                    console.log(e), e.data ? u(e.data._data.boxList) : i(e);
                },
                fail: function(e) {
                    console.log(e);
                }
            });
        });
    }
};