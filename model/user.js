Object.defineProperty(exports, "__esModule", {
    value: !0
});

var e = require("../config/index"), n = function(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}(require("../util/utils"));

exports.default = {
    wxOpenInfo: {},
    userInfo: {},
    getWxOpenInfo: function() {
        var e = this, n = this;
        return new Promise(function(t, o) {
            void 0 !== e.wxOpenInfo.nickName && t(e.wxOpenInfo), wx.getUserInfo({
                lang: "zh_CN",
                withCredentials: !1,
                success: function(e) {
                    var r = e.userInfo;
                    void 0 !== r ? (n.wxOpenInfo = r, wx.setStorageSync("wxOpenInfo", r), t(r)) : o(e);
                }
            });
        });
    },
    getWxCode: function() {
        return new Promise(function(e, n) {
            wx.login({
                success: function(t) {
                    t.code ? e(t.code) : n(t);
                },
                fail: function(e) {
                    n(e);
                }
            });
        });
    },
    doLogin: function() {
        var e = this;
        return new Promise(function(n, t) {
            e.getWxCode().then(function(n) {
                return e.getWxOpenId(n);
            }).then(function(n) {
                return e.getWxUserInfo(n);
            }).then(function(n) {
                e.saveUserInfo(n);
            }).catch(function(e) {
                console.log(e);
            });
        });
    },
    getWxOpenId: function(t) {
        return new Promise(function(o, r) {
            n.default.get({
                url: e.url.getWxOpenId + "&code=" + t,
                success: function(e) {
                    var n = e.data.data.openid;
                    o(n);
                }
            });
        });
    },
    getWxUserInfo: function(e) {
        return new Promise(function(n, t) {
            wx.getUserInfo({
                lang: "zh_CN",
                success: function(t) {
                    t.openid = e, n(t);
                },
                fail: function() {
                    console.log("getUserInfo Fail!!!");
                }
            });
        });
    },
    saveUserInfo: function(t) {
        var o = this, r = t.openid, a = t.rawData, i = t.userInfo;
        n.default.post({
            url: e.url.saveUserInfo,
            data: {
                openid: r,
                rawData: a
            },
            success: function(e) {
                var n = e.data.data.uid, t = e.data.data.phone;
                i.uid = n, i.phone = t, i.openid = r, o.setUserInfo(i);
            }
        });
    },
    login: function(t, o) {
        var r = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 2;
        arguments.length > 3 && void 0 !== arguments[3] && arguments[3];
        return new Promise(function(a, i) {
            console.log(e.url.authUserLogin), n.default.post({
                url: e.url.authUserLogin,
                data: {
                    openid: t || "",
                    auth_type: r || "",
                    unionid: o || "",
                    valid_days: 1
                },
                success: function(e) {
                    0 == (e = e.data).retcode ? a(e.data) : i(e.retinfo);
                },
                fail: function(e) {
                    console.log(e);
                }
            });
        });
    },
    getCode: function(t) {
        var o = t.tel;
        return new Promise(function(t, r) {
            n.default.post({
                url: e.url.getCode,
                data: {
                    tel: o
                },
                success: function(e) {
                    t(e.data);
                }
            });
        });
    },
    bindTelLogin: function(t) {
        console.log("tttttt", t);
        var o = t.tel, r = t.code, a = t.openid, i = t.uid;
        return new Promise(function(t, u) {
            n.default.post({
                url: e.url.bindTelLogin,
                data: {
                    tel: o,
                    code: r,
                    openid: a,
                    uid: i
                },
                success: function(e) {
                    0 == (e = e.data).errcode ? t(e.data) : u(e.errmsg);
                }
            });
        });
    },
    authUserUnbindTel: function(t) {
        var o = t.uid, r = t.userkey, a = t.openid;
        return new Promise(function(t, i) {
            n.default.post({
                url: e.url.authUserUnbindTel,
                data: {
                    uid: o,
                    openid: a,
                    userkey: r
                },
                success: function(e) {
                    0 == (e = e.data).retcode ? t(e.data) : i(e.retinfo);
                }
            });
        });
    },
    setUserInfo: function() {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
        this.userInfo = e;
        try {
            wx.setStorageSync("userInfo", e);
        } catch (e) {
            console.log("/model/user", "设置账户信息报错", e);
        }
    },
    getUserInfo: function() {
        return this.userInfo;
    },
    setWxToken: function() {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
        try {
            wx.setStorageSync("wxToken", e);
        } catch (e) {
            console.log("/model/user", "设置微信授权信息报错", e);
        }
    },
    getWxToken: function() {
        var e = {};
        try {
            var n = wx.getStorageSync("wxToken");
            n && (e = n);
        } catch (e) {
            console.log("/model/user", "从本地获取微信开放信息报错", e);
        }
        return e;
    },
    getSetting: function() {
        n.default.get({
            url: e.url.getSetting,
            data: {},
            success: function(e) {
                wx.setStorageSync("setting", e.data.data.setting), wx.setStorageSync("flash_index", e.data.data.flash_index), 
                wx.setStorageSync("class_index", e.data.data.class_index), wx.setStorageSync("comment_index", e.data.data.comment_index), 
                wx.setStorageSync("hotlist", e.data.data.hotlist);
            }
        });
    }
};