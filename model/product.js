Object.defineProperty(exports, "__esModule", {
    value: !0
});

var t = require("../config/index"), e = function(t) {
    return t && t.__esModule ? t : {
        default: t
    };
}(require("../util/utils"));

exports.default = {
    category: function() {
        return new Promise(function(a, r) {
            e.default.get({
                url: t.url.categories,
                success: function(t) {
                    t.data ? a(t.data.data) : r(t.errmsg);
                }
            });
        });
    },
    brands: function(a) {
        var r = a.cid;
        return new Promise(function(a, n) {
            e.default.get({
                url: t.url.brands,
                data: {
                    classId: r
                },
                success: function(t) {
                    t.data ? a(t.data.data) : n(t.errmsg);
                }
            });
        });
    },
    products: function(a) {
        var r = a.cid, n = a.bid, u = a.num;
        return new Promise(function(a, d) {
            e.default.get({
                url: t.url.products,
                data: {
                    classId: r,
                    brandId: n,
                    pageIndex: u
                },
                success: function(t) {
                    t.data ? a(t.data.data) : d(t.errmsg);
                }
            });
        });
    },
    hotlist: function(a) {
        var r = a.cid;
        return new Promise(function(a, n) {
            e.default.get({
                url: t.url.hots,
                data: {
                    classId: r
                },
                success: function(t) {
                    t.data ? a(t.data.data) : n(t.errmsg);
                }
            });
        });
    },
    productInfo: function(a) {
        var r = a.productId;
        return new Promise(function(a, n) {
            e.default.get({
                url: t.url.productInfo + "&pid=" + r,
                success: function(t) {
                    t.data ? a(t.data.data) : n(t.errmsg);
                }
            });
        });
    },
    evaluate: function(a) {
        return new Promise(function(r, n) {
            e.default.get({
                url: t.url.price + "&id=" + a.productId + "&selects=" + a.selects,
                success: function(t) {
                    t.data ? r(t.data.data) : n(t.data.errmsg);
                }
            });
        });
    },
    search: function(a) {
        return new Promise(function(r, n) {
            e.default.get({
                url: t.url.search + "&keyword=" + a.key,
                data: {
                    pageIndex: a.pageIndex || 1
                },
                success: function(t) {
                    t.data ? r(t.data.data) : n(t.errmsg);
                }
            });
        });
    },
    priceHistory: function(a) {
        return new Promise(function(r, n) {
            e.default.get({
                url: "" + t.url.priceHistory + a.price + "/" + a.productId + "/",
                success: function(t) {
                    t.data ? r(t.data.data) : n(t);
                }
            });
        });
    },
    queryModel: function(a, r) {
        return new Promise(function(n, u) {
            var d = e.default.createAppParams("evamap-queryModel", {
                modelArray: [ {
                    model: r,
                    brand: a
                } ]
            });
            wx.request({
                url: t.url.modelMap,
                header: {
                    "Content-Type": "application/json"
                },
                method: "post",
                data: JSON.stringify(d),
                success: function(t) {
                    console.log(t), t.data ? n(t.data.body.data) : u(t);
                },
                fail: function(t) {
                    console.log(t);
                }
            });
        });
    }
};