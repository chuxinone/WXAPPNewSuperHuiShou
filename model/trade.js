Object.defineProperty(exports, "__esModule", {
    value: !0
});

var e = require("../config/index"), t = function(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}(require("../util/utils"));

exports.default = {
    sfCity: function() {
        return new Promise(function(r, u) {
            t.default.get({
                url: e.url.sfCity,
                success: function(e) {
                    0 == (e = e.data).ret ? r(e.data) : u(e.retinfo);
                }
            });
        });
    },
    visitTime: function() {
        return new Promise(function(r, u) {
            t.default.get({
                url: e.url.support_visit_time,
                success: function(e) {
                    0 == (e = e.data).errcode ? r(e.data) : u(e.errmsg);
                }
            });
        });
    },
    hsbCity: function() {
        return new Promise(function(r, u) {
            t.default.get({
                url: e.url.hsbCity,
                success: function(e) {
                    0 == (e = e.data).errcode ? r(e.data) : u(e.errmsg);
                }
            });
        });
    }
};