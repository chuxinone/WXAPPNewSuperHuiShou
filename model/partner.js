function e(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}

Object.defineProperty(exports, "__esModule", {
    value: !0
});

var t = e(require("../util/md5")), r = e(require("../util/utils")), i = {
    trade: function(e) {
        var i = "order=" + (e = {
            sid: e.sid ? parseInt(e.sid) : 0,
            uid: e.uid ? parseInt(e.uid) : 0,
            pid: e.pid,
            order: e.orderNum,
            tel: e.tel,
            product: e.productName,
            productid: parseInt(e.productId),
            pmoney: 100 * parseInt(e.price),
            time: parseInt(new Date().getTime() / 1e3)
        }).order + "&pmoney=" + e.pmoney + "&time=" + e.time + "&key=6k$Mu86@AN9";
        return e.token = (0, t.default)(i), new Promise(function(t, i) {
            r.default.post({
                url: "https://www.icarephone.com/business/hsb/recover_order",
                data: e,
                success: function(e) {
                    t(e);
                },
                fail: function(e) {
                    i(e);
                }
            });
        });
    }
}, d = {
    wxeb46e3105e6634c5: i,
    wx3b23b2ebeec25313: i,
    1173: i
}, o = {
    appid: "",
    extraData: {},
    pid: ""
};

exports.default = {
    store: o,
    setPartnerInfo: function(e) {
        var t = e.appid, r = e.extraData;
        this.store.appid = t, this.store.extraData = r, this.store.pid = r.pid ? r.pid : 1196;
    },
    getPartnerApi: function() {
        return d[this.store.pid];
    }
};