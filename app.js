function e(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}

var t = e(require("./model/user")), n = e(require("./model/partner"));

App({
    globalData: {
        cid: 1,
        bid: -1,
        pid: 1196,
        cdn: "",
        isConnected: !0,
        isIphoneX: !1,
        couponsList: []
    },
    onHide: function() {
        this.clearUserInfo(), n.default.setPartnerInfo({
            appId: "",
            extraData: {}
        });
    },
    clearUserInfo: function() {
        wx.removeStorage({
            key: "userInfo"
        });
    },
    getDetectTimer: function() {
        var e = this;
        setTimeout(function() {
            e.detectWxOauth();
        }, 500);
    },
    detectWxOauth: function() {
        var e = this;
        wx.getSetting({
            success: function(t) {
                var n = t.authSetting["scope.userInfo"];
                !1 !== n ? void 0 === n && e.getDetectTimer() : wx.showModal({
                    title: "提示",
                    content: "必须授权登录才能使用",
                    success: function(t) {
                        t.confirm ? wx.openSetting() : e.getDetectTimer();
                    }
                });
            },
            fail: function(e) {
                console.log(e);
            }
        });
    },
    onLaunch: function() {
        var e = Page;
        Page = function(t) {
            return t.onShareAppMessage || (t.onShareAppMessage = function() {
                return {
                    title: "",
                    path: "/"
                };
            }), e(t);
        };
    },
    onShow: function(e) {
        var t = this;
        if (this.getDetectTimer(), 1037 == e.scene) {
            var o = e.referrerInfo.appId, i = e.referrerInfo.extraData;
            "string" == typeof i && (i = JSON.parse(i)), n.default.setPartnerInfo({
                appId: o,
                extraData: i
            }), this.globalData.pid = i.pid ? i.pid : 1196;
        }
        wx.getSystemInfo({
            success: function(e) {
                var n = e.model;
                console.log(n), -1 != n.search("iPhone X") && (t.globalData.isIphoneX = !0);
            }
        }), this.getSetting(), this.login();
    },
    login: function() {
        t.default.doLogin();
    },
    getSetting: function() {
        t.default.getSetting();
    },
    siteInfo: require("siteinfo.js")
});