function t(t) {
    return t && t.__esModule ? t : {
        default: t
    };
}

t(require("../../../model/cabinet"));

var a = t(require("../../../util/amap-wx")), e = require("../../../config/index"), n = t(require("../../../util/utils")), o = void 0;

Page({
    data: {
        key: "",
        searchList: [],
        history: []
    },
    onLoad: function(t) {
        o = this;
    },
    onShow: function() {
        wx.hideShareMenu(), o = this;
        var t = wx.getStorageSync("searchPlace");
        o.setData({
            history: t.length ? t : []
        }), o.data.key && o.loadMore();
    },
    loadMore: function() {
        if (o.data.key.length) {
            var t = o.data.searchList, n = o.data.key;
            new a.default.AMapWX({
                key: e.gdKey
            }).getInputtips({
                keywords: n,
                location: "",
                success: function(a) {
                    console.log(a), t = t.concat(a.tips), o.setData({
                        searchList: t
                    });
                }
            });
        }
    },
    throttle: n.default.throttle(function() {
        o.setData({
            searchList: []
        }), o.loadMore();
    }, 300),
    handleKey: function(t) {
        o.setData({
            key: t.detail.value
        }), o.onKeyChanged();
    },
    onKeyChanged: function() {
        o.throttle();
    },
    navigateBackFunc: function(t) {
        var a = getCurrentPages(), e = a[a.length - 2];
        console.log(e);
        var n = t.split(",")[0], o = t.split(",")[1];
        e.setData({
            latitude: o,
            longitude: n
        });
    },
    switchPageOnly: function(t) {
        var a = t.currentTarget.dataset;
        console.log(a), o.navigateBackFunc(a.location), wx.navigateBack();
    },
    switchPage: function(t) {
        var a = t.currentTarget.dataset, e = o.data.history, n = {
            name: a.name,
            location: a.location
        };
        e.unshift(n), e = e.splice(0, 6), wx.setStorage({
            key: "searchPlace",
            data: e
        }), o.setData({
            history: e
        }), o.navigateBackFunc(a.location), wx.navigateBack();
    },
    clearHistory: function() {
        o.setData({
            history: []
        }), wx.setStorage({
            key: "searchPlace",
            data: []
        });
    },
    clearKey: function() {
        o.setData({
            key: ""
        });
    }
});