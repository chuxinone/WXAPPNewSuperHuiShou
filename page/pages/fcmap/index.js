function t(t) {
    return t && t.__esModule ? t : {
        default: t
    };
}

require("../../../config/index");

var e = t(require("../../../util/utils")), a = t(require("../../../model/cabinet")), n = void 0;

Page({
    data: {
        longitude: "",
        latitude: "",
        markers: [],
        includePoints: [],
        scale: 16,
        open: !1,
        textData: [],
        mapHeight: 0,
        footerMaxHeight: 500,
        winHeight: 0,
        controls: [ {
            id: 1,
            iconPath: "../../../img/fcmap/dingweitu.png",
            position: {
                left: 1.5,
                top: 6.5,
                width: 50,
                height: 50
            },
            clickable: !0
        } ],
        toSelf: !0,
        devicePer: 2
    },
    onLoad: function(t) {
        var a = void 0, o = void 0;
        n = this, t.location ? (a = t.location.split(",")[0], o = t.location.split(",")[1], 
        n.setData({
            longitude: a,
            latitude: o
        })) : e.default.getLocation().then(function(t) {
            console.log(t);
            var e = t[0].longitude, a = t[0].latitude;
            n.setData({
                longitude: e,
                latitude: a
            });
        });
    },
    controltap: function(t) {
        var e = n.data.controls;
        n.data.toSelf ? (e[0].iconPath = "../../../img/fcmap/dangqianweizhi.png", n.mapCtx.moveToLocation(), 
        n.setData({
            toSelf: !1,
            controls: e
        })) : (e[0].iconPath = "../../../img/fcmap/dingweitu.png", n.mapCtx.includePoints({
            padding: [ 50, 50, 50, 50 ],
            points: n.data.includePoints
        }), n.setData({
            toSelf: !0,
            scale: 16,
            controls: e
        }));
    },
    takeOpen: function(t) {
        0 !== n.data.textData.length && n.setData({
            open: !0,
            mapHeight: n.data.winHeight - 500 - 20 * n.data.devicePer,
            footerMaxHeight: 500,
            scale: 15
        });
    },
    takeMarker: function(t) {
        var e = t.currentTarget.dataset, a = n.data.markers, o = n.data.devicePer;
        a.forEach(function(t, n) {
            n > 0 && (n == e.id + 1 ? (a[n].iconPath = "../../../img/fcmap/fengchao-dt-xuanzhong.png", 
            a[n].width = 35, a[n].height = 35) : (a[n].iconPath = "../../../img/fcmap/fengchao-dt.png", 
            a[n].width = 28, a[n].height = 28));
        }), n.setData({
            markers: a,
            open: !1,
            mapHeight: n.data.winHeight - 45 * o,
            footerMaxHeight: 45 * o,
            scale: 16
        });
        var i = n.data.controls;
        i[0].iconPath = "../../../img/fcmap/dingweitu.png", n.mapCtx.includePoints({
            padding: [ 50, 50, 50, 50 ],
            points: n.data.includePoints
        }), n.setData({
            toSelf: !0,
            controls: i
        });
    },
    regionchange: function(t) {},
    switchPage: function(t) {
        var e = t.currentTarget.dataset;
        wx.navigateTo({
            url: e.url
        });
    },
    onReady: function() {
        n.mapCtx = wx.createMapContext("myMap"), n.mapCtx.getScale({
            success: function(t) {
                console.log(t), n.setData({
                    scale: t.scale
                });
            }
        });
    },
    onShow: function() {
        wx.hideShareMenu();
        var t = n.data.longitude, e = n.data.latitude;
        wx.getSystemInfo({
            success: function(t) {
                console.log(t);
                var e = n.data.controls;
                e[0].position.left = t.windowWidth - 53;
                var a = 750 / t.screenWidth, o = t.windowHeight * a - 55 * a;
                n.setData({
                    winHeight: o,
                    mapHeight: o - 45 * a,
                    controls: e,
                    open: !1,
                    devicePer: a
                });
            }
        });
        var o = [], i = [], c = [];
        o.push({
            longitude: t,
            latitude: e,
            iconPath: "../../../img/fcmap/dingwei.png",
            width: 21,
            height: 30
        }), i.push({
            longitude: t,
            latitude: e
        }), n.setData({
            longitude: t,
            latitude: e
        }), a.default.getBoxInfo(t, e, 500).then(function(t) {
            console.log(t), t && 0 != t.length && t.forEach(function(t, e) {
                o.push({
                    longitude: t.longitude,
                    latitude: t.latitude,
                    iconPath: "../../../img/fcmap/fengchao-dt.png",
                    width: 28,
                    height: 28
                }), i.push({
                    longitude: t.longitude,
                    latitude: t.latitude
                }), c.push({
                    name: t.address,
                    bigBoxCount: t.bigBoxCount,
                    middleBoxCount: t.middleBoxCount,
                    smallBoxCount: t.smallBoxCount
                });
            }), n.setData({
                markers: o,
                includePoints: i,
                textData: c
            }), n.mapCtx = wx.createMapContext("myMap"), n.mapCtx.getScale({
                success: function(t) {
                    console.log(t), n.setData({
                        scale: t.scale
                    });
                }
            });
        }), n.setData({
            markers: o,
            includePoints: i,
            textData: c
        }), n.mapCtx = wx.createMapContext("myMap"), n.mapCtx.getScale({
            success: function(t) {
                console.log(t), n.setData({
                    scale: t.scale
                });
            }
        });
    },
    onHide: function() {},
    onUnload: function() {},
    onPullDownRefresh: function() {},
    onReachBottom: function() {},
    onShareAppMessage: function() {}
});