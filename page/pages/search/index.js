function t(t) {
    return t && t.__esModule ? t : {
        default: t
    };
}

var e = t(require("../../../model/product")), a = t(require("../../../util/utils")), o = void 0;

Page({
    data: {
        key: "",
        hotList: [],
        searchList: [],
        history: [],
        index: 0,
        hasMore: !0,
        isShowNoMoreText: !1,
        isShowLoadText: void 0,
        timeId: null
    },
    onLoad: function(t) {
        o = this, "输入您要搜索的机型" !== t.key && o.setData({
            key: t.key
        });
    },
    onShow: function() {
        o = this;
        var t = wx.getStorageSync("search"), e = wx.getStorageSync("hotList");
        o.setData({
            history: t.length ? t : [],
            hotList: e.length ? e : []
        }), o.data.key && o.loadMore();
    },
    loadMore: function() {
        if (!o.data.hasMore) return o.setData({
            isShowNoMoreText: !0
        }), void setTimeout(function() {
            o.setData({
                isShowNoMoreText: !1
            });
        }, 2e3);
        if (o.data.key.length) {
            var t = ++o.data.index, a = o.data.searchList;
            o.setData({
                isShowLoadText: !0
            }), e.default.search({
                key: encodeURIComponent(o.data.key),
                pageIndex: t
            }).then(function(t) {
                var e = t.pdtcount > (a = a.concat(t.productlist)).length;
                o.setData({
                    hasMore: e,
                    searchList: a,
                    isShowLoadText: !1
                });
            }, function(t) {
                console.log(t);
            });
        }
    },
    throttle: a.default.throttle(function() {
        o.setData({
            searchList: [],
            index: 0,
            hasMore: !0,
            isShowNoMoreText: !1,
            isShowLoadText: void 0
        }), o.loadMore();
    }, 300),
    handleKey: function(t) {
        o.setData({
            key: t.detail.value
        }), o.onKeyChanged();
    },
    onKeyChanged: function() {
        o.throttle();
    },
    switchPage: function(t) {
        var e = t.currentTarget.dataset, n = o.data.history;
        if (e.log) {
            var i = {
                id: e.id,
                name: e.name
            };
            a.default.inArray(n, i, "id") || (n.unshift(i), n = n.splice(0, 10), wx.setStorage({
                key: "search",
                data: n
            }), o.setData({
                history: n
            }));
        }
        wx.navigateTo({
            url: e.url
        });
    },
    clearHistory: function() {
        o.setData({
            history: []
        }), wx.setStorage({
            key: "search",
            data: []
        });
    },
    clearKey: function() {
        o.setData({
            key: ""
        });
    },
    onShareAppMessage: function(t) {
        return "button" === t.from && console.log(t.target), {
            title: "专业的手机数码估价回收平台",
            path: "page/pages/search/index",
            success: function(t) {},
            fail: function(t) {}
        };
    }
});