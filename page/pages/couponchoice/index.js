function e(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}

e(require("../../../util/utils"));

var t = e(require("../../../model/coupon")), o = e(require("../../../model/user")), n = void 0, a = getApp();

Page({
    data: {
        token: "",
        couponList: [],
        productId: "",
        price: "",
        choicedIndex: -1
    },
    onLoad: function(e) {
        n = this;
        var t = e.productId, o = e.price, i = e.choicedIndex;
        n.setData({
            productId: t,
            price: o,
            choicedIndex: i,
            couponList: a.globalData.couponsList
        });
    },
    onShow: function() {
        wx.hideShareMenu(), wx.setNavigationBarTitle({
            title: "我的优惠券"
        }), wx.setNavigationBarColor({
            frontColor: "#000000",
            backgroundColor: "#ffffff"
        });
        var e = o.default.getUserInfo();
        n.setData({
            userInfo: e
        });
    },
    choiceCoupon: function(e) {
        var t = e.currentTarget.dataset, o = n.data.couponList, a = n.data.choicedIndex;
        if (2 != t.canuse) {
            a == t.index ? a = -1 : o.map(function(e, o) {
                o == t.index && (a = o);
            }), n.setData({
                choicedIndex: a
            });
            var i = getCurrentPages(), d = i[i.length - 2];
            console.log(d), d.setData({
                choicedIndex: a
            });
        }
    },
    handleToken: function(e) {
        n.setData({
            token: e.detail.value
        });
    },
    loadCoupons: function() {
        var e = o.default.getUserInfo(), i = [], d = n.data.choicedIndex;
        t.default.canUse({
            userId: e.old_uid,
            proId: n.data.productId,
            price: 100 * n.data.price
        }).then(function(e) {
            if ((i = e.couponsList).length) {
                i.map(function(e, t) {
                    return e.value = parseInt(e.couponsValue) / 100, e.minQuation = parseInt(e.minQuation) / 100, 
                    e.deadline = e.couponsValidity.substring(0, 10), 1 == e.isCanUse && 0 == t && (d = 0), 
                    e;
                }), a.globalData.couponsList = i, n.setData({
                    choicedIndex: d,
                    couponList: i
                });
                var t = getCurrentPages(), o = t[t.length - 2];
                console.log(o), o.setData({
                    choicedIndex: d
                });
            }
        });
    },
    addCoupon: function() {
        t.default.gain({
            id: n.data.token,
            userId: n.data.userInfo.old_uid
        }).then(function(e) {
            wx.showToast({
                title: "添加成功"
            }), n.loadCoupons(), n.setData({
                token: ""
            });
        }, function(e) {
            wx.showModal({
                title: "提示",
                content: e,
                showCancel: !1
            }), n.setData({
                token: ""
            });
        });
    }
});