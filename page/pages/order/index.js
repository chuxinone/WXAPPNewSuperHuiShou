function t(t) {
    return t && t.__esModule ? t : {
        default: t
    };
}

var e = t(require("../../../model/order")), o = t(require("../../../model/user")), n = t(require("../../../util/utils.js")), a = void 0, i = getApp();

Page({
    data: {
        userInfo: {},
        orderInfo: {},
        coupon_list: [],
        cdn: i.globalData.cdn,
        location: "",
        showAll: !1,
        onlyOne: !0,
        onlyTwo: !0,
        alertTip: !1,
        isIphoneX: i.globalData.isIphoneX,
        setting: {}
    },
    onShow: function() {
        wx.hideShareMenu();
        var t = wx.getStorageSync("setting");
        this.setData({
            setting: t
        });
    },
    onLoad: function(t) {
        var i = this;
        a = this;
        var r = o.default.getUserInfo(), s = [];
        wx.setNavigationBarTitle({
            title: "订单详情"
        }), n.default.setWhiteNavBar(), e.default.order({
            uid: r.uid,
            openid: r.openid,
            orderid: t.orderid
        }).then(function(t) {
            i.setData({
                orderInfo: t,
                coupon_list: s,
                userInfo: r,
                onlyTwo: t.sfRouteInfo && t.sfRouteInfo.length <= 2 || t.fcRouteInfo && t.fcRouteInfo.length <= 2
            });
        }, function(t) {
            console.log(t);
        });
    },
    closeMue: function() {
        this.setData({
            alertTip: !1
        });
    },
    showDetails: function() {
        var t = a.data.orderInfo.order_status_list.length <= 1;
        this.setData({
            alertTip: !0,
            onlyOne: t
        });
    },
    showAll: function() {
        this.setData({
            showAll: !0
        });
    },
    hideSome: function() {
        this.setData({
            showAll: !1
        });
    },
    switchPage: function(t) {
        var e = t.currentTarget.dataset;
        wx.navigateTo({
            url: e.url + "?location=" + a.data.location
        });
    }
});