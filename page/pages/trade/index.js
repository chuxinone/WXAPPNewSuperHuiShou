function e(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}

var a = e(require("../../../model/trade")), t = e(require("../../../model/order")), d = e(require("../../../model/user")), s = e(require("../../../model/coupon")), r = e(require("../../../util/utils")), n = void 0;

getApp(), Page({
    data: {
        ways: [ {
            name: "顺丰上门取件",
            way: "sf",
            iconBlack: "../../../img/trade/sf_black.svg",
            iconWhite: "../../../img/trade/sf_white.svg"
        }, {
            name: "上门回收（免费）",
            way: "visit",
            iconBlack: "../../../img/trade/visit_black.svg",
            iconWhite: "../../../img/trade/visit_white.svg"
        } ],
        way: "sf",
        sf: {
            addr: {
                data: [],
                indexs: [ 0, 0, 0 ],
                selects: [ [], [], [] ],
                value: ""
            },
            date: {
                data: [],
                indexs: [ 0, 0 ],
                selects: [ [], [] ],
                value: ""
            }
        },
        hsb: {
            addr: {
                data: [],
                indexs: [ 0, 0 ],
                selects: [ [], [] ],
                value: ""
            },
            date: {
                data: [],
                indexs: [ 0, 0 ],
                selects: [ [], [] ],
                value: ""
            }
        },
        street: "",
        nickname: "",
        tel: "",
        coupon: {},
        userInfo: {},
        telFocus: !1,
        nicknameFocus: !1,
        options: {}
    },
    onLoad: function(e) {
        var t = (n = this).data.sf, d = n.data.hsb;
        a.default.sfCity().then(function(e) {
            var a = {};
            t.addr.data = e, t.addr.selects[0] = e, (a = e[0]).sub && (t.addr.selects[1] = a.sub), 
            (a = a.sub[0]).sub && (t.addr.selects[2] = a.sub), t.addr.value = n.sfAddrFormat(t), 
            n.setData({
                sf: t
            });
        }), a.default.visitTime().then(function(e) {
            t.date.data = e.sf, t.date.selects[0] = e.sf.date, t.date.selects[1] = e.sf.time[0], 
            t.date.value = n.formatSfDate(t), d.date.data = e.hsb, d.date.selects[0] = e.hsb.date, 
            d.date.selects[1] = e.hsb.time[0], d.date.value = n.formatHsbDate(d), n.setData({
                sf: t,
                hsb: d
            });
        }), a.default.hsbCity().then(function(e) {
            d.addr.data = e, d.addr.selects[0] = e, d.addr.selects[1] = e[0].sub, d.addr.value = n.hsbAddrFormat(d), 
            n.setData({
                hsb: d
            });
        }), n.setData({
            options: e
        });
    },
    onShow: function() {
        wx.hideShareMenu();
        var e = d.default.getUserInfo();
        n.data.tel, e && e.tel ? n.setData({
            userInfo: e,
            tel: e.tel
        }) : n.setData({
            userInfo: e
        }), e.tel && s.default.page({
            uid: e.us_uid,
            userkey: e.userkey
        }).then(function(e) {
            var a = r.default.getCurPageOpt(), t = r.default.getAvailableCoupon(e, parseInt(a.price));
            t && n.setData({
                coupon: t
            });
        });
    },
    sfAddrFormat: function(e) {
        for (var a = [], t = 0; t < 3; t++) e.addr.selects[t].length && a.push(e.addr.selects[t][e.addr.indexs[t]].name);
        return a.join(" ");
    },
    hsbAddrFormat: function(e) {
        for (var a = [], t = 0; t < 2; t++) e.addr.selects[t].length && a.push(e.addr.selects[t][e.addr.indexs[t]].name);
        return a.join(" ");
    },
    handleSfAddr: function(e) {
        var a = this.data.sf;
        a.addr.indexs = e.detail.value, this.setData({
            sf: a
        });
    },
    handleSfAddrCol: function(e) {
        var a = n.data.sf, t = e.detail.value, d = e.detail.column;
        switch (d) {
          case 0:
            a.addr.selects[1] = a.addr.data[t].sub, a.addr.selects[1][0].sub ? a.addr.selects[2] = a.addr.selects[1][0].sub : a.addr.selects[2] = [], 
            a.addr.indexs[d + 1] = 0, a.addr.indexs[d + 2] = 0;
            break;

          case 1:
            a.addr.selects[d][t].sub ? a.addr.selects[2] = a.addr.selects[d][t].sub : a.addr.selects[2] = [], 
            a.addr.indexs[d + 1] = 0;
        }
        a.addr.indexs[d] = t, a.addr.value = n.sfAddrFormat(a), this.setData({
            sf: a
        });
    },
    handleHsbAddrCol: function(e) {
        var a = n.data.hsb, t = e.detail.value, d = e.detail.column;
        switch (d) {
          case 0:
            a.addr.selects[1] = a.addr.data[t].sub, a.addr.indexs[d] = t, a.addr.indexs[d + 1] = 0;
            break;

          case 1:
            a.addr.indexs[1] = t;
        }
        a.addr.value = n.hsbAddrFormat(a), this.setData({
            hsb: a
        });
    },
    handleSfDateCol: function(e) {
        var a = n.data.sf, t = e.detail.value, d = e.detail.column;
        0 == d && (a.date.selects[1] = a.date.data.time[t]), a.date.indexs[d] = t, a.date.value = n.formatSfDate(a), 
        this.setData({
            sf: a
        });
    },
    handleHsbDateCol: function(e) {
        var a = n.data.hsb, t = e.detail.value, d = e.detail.column;
        0 == d && (a.date.selects[1] = a.date.data.time[t]), a.date.indexs[d] = t, a.date.value = this.formatHsbDate(a), 
        this.setData({
            hsb: a
        });
    },
    formatSfDate: function(e) {
        for (var a = [], t = 0; t < 2; t++) e.date.selects[t].length && a.push(e.date.selects[t][e.date.indexs[t]]);
        return a.join(" ");
    },
    formatHsbDate: function(e) {
        for (var a = [], t = 0; t < 2; t++) e.date.selects[t].length && a.push(e.date.selects[t][e.date.indexs[t]]);
        return a.join(" ");
    },
    switchWay: function(e) {
        var a = e.currentTarget.dataset, t = n.data.options;
        if ("visit" === a.way) {
            var d = new Date("Feb 22, 2018 00:00:00").getTime();
            if (new Date().getTime() < d) return void n.showModel("由于春节放假，2018年2月22号以前无工作人员上门，回收宝祝您春节快乐，阖家幸福！");
            if (parseInt(t.price) < 100) return void n.showModel("未满100元不支持上门回收");
            if (2 == t.classId) return void n.showModel("笔记本暂不支持上门回收");
        }
        n.setData({
            way: a.way
        });
    },
    validParam: function(e) {
        return e && e.length > 0;
    },
    showModel: function(e) {
        wx.showModal({
            title: "提示",
            content: e,
            showCancel: !1
        });
    },
    takeHsbVisitOrder: function(e) {
        var a = n.data.hsb, d = n.data.street, s = a.addr.value + d, r = a.date.value;
        if (!n.validParam(d)) return n.showModel("请填写详细地址"), !1;
        if (!n.validParam(s)) return n.showModel("请选择城市"), !1;
        if (!n.validParam(r)) return n.showModel("请选择预约时间"), !1;
        var i = a.addr.selects[0][a.addr.indexs[0]].id, o = new Date((r.substr(0, 16) + ":00").replace(/-/g, "/")).getTime() / 1e3;
        e.ordertype = "visit", e.address = s, e.regionId = i, e.visitTime = o, e.address = s, 
        e.displayVisitTime = r, t.default.take(e).then(function(a) {
            Object.assign(e, a), wx.reLaunch({
                url: "../success/index?orderInfo=" + JSON.stringify(e)
            });
        }, function(e) {
            console.log(e);
        });
    },
    takeSfOrder: function(e) {
        var a = n.data.sf, d = n.data.street, s = a.addr.value, r = a.date.value;
        if (!n.validParam(d)) return n.showModel("请填写详细地址");
        if (!n.validParam(s)) return n.showModel("请选择城市");
        if (!n.validParam(r)) return n.showModel("请选择预约时间");
        var i = new Date(r.replace(/-/g, "/")).getTime() / 1e3, o = s.split(" ");
        e.ordertype = "post", t.default.take(e).then(function(a) {
            e.ordernum = a.ordernum, e.orderid = a.orderid, e.sendtime = i, e.province = o[0] || "", 
            e.city = o[1] || "", e.county = o[2] || "", e.addr = s + " " + d, e.displayVisitTime = r, 
            wx.reLaunch({
                url: "../success/index?orderInfo=" + JSON.stringify(e)
            });
        });
    },
    submitOrder: function() {
        var e = n.data.tel, a = n.data.nickname;
        if (a.length < 2) return wx.showModal({
            title: "提示",
            content: "联系人字符长度不能小于2",
            showCancel: !1
        });
        if (!r.default.isMobile(e)) return wx.showModal({
            title: "提示",
            content: "手机号码格式不正确",
            showCancel: !1
        });
        var t = n.data.options, s = d.default.getUserInfo(), i = d.default.getWxToken(), o = n.data.coupon, l = {
            selects: t.selects,
            itemid: t.productId,
            uid: s.us_uid,
            openid: i.openid,
            type: 9,
            olduid: s.old_uid,
            userkey: s.userkey,
            nickname: a,
            tel: e
        };
        l.productName = t.productName, l.price = t.price, Object.keys(o).length && (l.couponId = o.couponID);
        var u = n.data.way;
        "sf" === u && n.takeSfOrder(l), "visit" === u && n.takeHsbVisitOrder(l);
    },
    showCoupDesc: function() {
        wx.showModal({
            title: "现金券说明",
            content: "系统自动使用您的可以使用的最大面值现金券",
            showCancel: !1
        });
    },
    handleName: function(e) {
        this.setData({
            nickname: e.detail.value
        });
    },
    handleTel: function(e) {
        this.setData({
            tel: e.detail.value
        });
    },
    handleStreet: function(e) {
        this.setData({
            street: e.detail.value
        });
    },
    onTelFocus: function(e) {
        n.setData({
            telFocus: !0
        });
    },
    onTelBlur: function(e) {
        n.setData({
            telFocus: !1
        });
    },
    onNicknameBlur: function(e) {
        n.setData({
            nicknameFocus: !1
        });
    },
    onNicknameFocus: function(e) {
        console.log(e), n.setData({
            nicknameFocus: !0
        });
    }
});