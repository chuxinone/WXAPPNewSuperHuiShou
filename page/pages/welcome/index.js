function t(t) {
    return t && t.__esModule ? t : {
        default: t
    };
}

t(require("../../../model/user"));

var e = t(require("../../../model/product")), n = void t(require("../../../util/utils")), o = getApp();

Page({
    data: {
        imgUrls: [ {
            url: "../couponget/index",
            src: "../../../img/banner_coupon.png"
        } ],
        iconSearch: "../../../img/icon-search.svg",
        cdn: o.globalData.cdn,
        productList: [],
        cateList: [ {
            cid: 1,
            bid: -1,
            text: "手机回收",
            imgSrc: "../../../img/icon-mobile-1.png"
        }, {
            cid: 3,
            bid: -1,
            text: "平板回收",
            imgSrc: "../../../img/icon-pad-1.png"
        }, {
            cid: 2,
            bid: -1,
            text: "笔记本回收",
            imgSrc: "../../../img/icon-notebook-1.png"
        } ],
        curModel: "iPhone5s",
        curModelInfo: null,
        setting: {
            liucheng_show: 1,
            fuwu_show: 1,
            flash_show: 1,
            class_show: 1,
            button_show: 1,
            comment_show: 1
        },
        commentData: []
    },
    onLoad: function() {},
    onShow: function() {
        var t = this, e = setInterval(function() {
            wx.getStorageSync("setting") && (t.getData(), clearInterval(e));
        }, 100);
    },
    getData: function() {
        var t = wx.getStorageSync("setting"), e = wx.getStorageSync("class_index"), n = wx.getStorageSync("flash_index"), o = wx.getStorageSync("comment_index"), a = wx.getStorageSync("hotlist");
        this.setData({
            setting: t,
            cateList: e,
            imgUrls: n,
            commentData: o,
            hotList: a
        }), wx.setNavigationBarTitle({
            title: t.app_title || "首页"
        });
    },
    onShareAppMessage: function(t) {
        var e = wx.getStorageSync("setting");
        return "button" === t.from && console.log(t.target), {
            title: "专业的手机数码估价回收平台" | e.share_title,
            path: "page/pages/welcome/index",
            success: function(t) {},
            fail: function(t) {}
        };
    },
    getProduct: function() {
        var t = wx.getStorageSync("hotList");
        t.length && n.setData({
            productList: t
        }), e.default.products({
            cid: 1,
            bid: -1,
            num: 1,
            pageSize: 8
        }).then(function(t) {
            var e = t.productlist.splice(0, 4);
            n.setData({
                productList: e
            }), wx.setStorage({
                key: "hotList",
                data: e
            });
        });
    },
    switchProducts: function(t) {
        var e = t.currentTarget.dataset;
        o.globalData.cid = e.cid, o.globalData.bid = e.bid, wx.navigateTo({
            url: "../products/index"
        });
    },
    getModelInfo: function(t) {
        e.default.search({
            key: encodeURIComponent(t),
            pageIndex: 20
        }).then(function(e) {
            e.productlist && e.productlist.length && e.productlist.forEach(function(e) {
                e.productname === t && n.setData({
                    curModelInfo: e
                });
            });
        }, function(t) {
            console.log(t);
        });
    },
    brandMap: function(t, n, o) {
        console.log(t), console.log(n), e.default.queryModel(t, n).then(function(t) {
            o(t);
        });
    },
    switchPage: function(t) {
        var e = t.currentTarget.dataset;
        wx.navigateTo({
            url: e.url
        });
    },
    kefu: function(t) {
        var e = t.currentTarget.dataset.phone;
        e && wx.makePhoneCall({
            phoneNumber: e
        });
    }
});