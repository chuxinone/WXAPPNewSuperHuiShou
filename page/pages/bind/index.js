function e(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}

var t = e(require("../../../model/user")), n = void e(require("../../../util/utils")), i = void 0;

getApp(), Page({
    data: {
        tel: "",
        code: "",
        openid: "",
        unionid: "",
        isValidTel: !1,
        counter: 0,
        options: {}
    },
    onLoad: function(e) {
        n = this, wx.setNavigationBarTitle({
            title: "绑定手机"
        });
        var i = t.default.getUserInfo();
        console.log("userinfo", i);
        var o = t.phone;
        n.setData({
            options: e,
            tel: o || "",
            openid: i.openid,
            uid: i.uid,
            isValidTel: o && 11 === o.length
        });
    },
    onShow: function() {
        wx.hideShareMenu();
    },
    getCode: function() {
        var e = n.data.tel;
        n.setData({
            counter: 30
        }), n.counting(), t.default.getCode({
            tel: e
        }).then(function(e) {
            var t = {
                title: "提示",
                icon: "loading"
            };
            0 == e.errcode ? (t.title = "发送短信成功", t.icon = "success") : t.title = e.errmsg, 
            wx.showToast(t);
        });
    },
    counting: function() {
        var e = n.data.counter;
        0 !== e && (i = setTimeout(function() {
            n.setData({
                counter: e - 1
            }), n.counting();
        }, 1e3));
    },
    handleTel: function(e) {
        var t = e.detail.value;
        this.setData({
            tel: t,
            isValidTel: t && 11 === t.length
        });
    },
    handleCode: function(e) {
        var t = e.detail.value;
        this.setData({
            code: t
        });
    },
    clearTel: function() {
        n.setData({
            tel: "",
            isValidTel: !1,
            code: ""
        });
    },
    submit: function() {
        n.data, n.bindTelLogin();
    },
    switchPage: function() {
        var e = n.data.options;
        console.log(e), e.redirectUrl ? wx.redirectTo({
            url: decodeURIComponent(e.redirectUrl)
        }) : wx.navigateBack({
            delta: 1
        });
    },
    bindTelLogin: function() {
        var e = n.data, i = e.tel;
        t.default.bindTelLogin({
            tel: e.tel,
            code: e.code,
            openid: e.openid,
            uid: e.uid
        }).then(function(e) {
            var o = t.default.getUserInfo();
            o.phone = i, t.default.setUserInfo(o), n.switchPage();
        }, function(e) {
            wx.showToast({
                title: e
            });
        });
    }
});