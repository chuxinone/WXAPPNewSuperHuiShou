var e = function(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}(require("../../../model/user")), n = void 0;

getApp(), Page({
    data: {
        openid: "",
        unionid: "",
        userInfo: {},
        wxOpenInfo: {},
        grid1Cols: [ {
            text: "我的订单",
            path: "../orders/index",
            icon: "../../../img/icon-order.png"
        }, {
            text: "邮寄地址",
            path: "../address/index",
            icon: "../../../img/icon-addr.png"
        } ]
    },
    onLoad: function() {
        n = this, wx.setNavigationBarTitle({
            title: "个人中心"
        });
    },
    onShow: function() {
        var n = this;
        wx.hideShareMenu();
        var t = e.default.getUserInfo();
        n.setData({
            wxOpenInfo: t
        });
    },
    login: function() {
        e.default.getWxOpenId().then(function(t) {
            e.default.setWxToken(t), e.default.login(t.openId, t.unionId).then(function(t) {
                e.default.setUserInfo(t), n.setData({
                    userInfo: t
                });
            }, function(t) {
                console.log(t), n.setData({
                    userInfo: {}
                }), e.default.setUserInfo({});
            });
        });
    },
    switchBind: function(e) {
        var n = e.currentTarget.dataset;
        wx.navigateTo({
            url: "../bind/index?type=" + n.type
        });
    },
    switchPage: function(e) {
        var t = e.currentTarget.dataset;
        n.data.userInfo;
        wx.navigateTo({
            url: t.url
        });
    },
    unbindTel: function() {
        var t = n.data, o = e.default.getUserInfo();
        wx.showModal({
            title: "提示",
            content: "您真的要取消吗？",
            success: function(a) {
                if (!a.confirm) return !1;
                e.default.authUserUnbindTel({
                    uid: o.us_uid,
                    userkey: o.userkey,
                    openid: t.openid
                }).then(function(t) {
                    e.default.setUserInfo({}), n.setData({
                        userInfo: {}
                    });
                }, function(e) {
                    console.log(e), wx.showModal({
                        title: "提示",
                        content: e,
                        showCancel: !1
                    });
                });
            }
        });
    },
    dialNumber: function(e) {
        wx.makePhoneCall({
            phoneNumber: "4000809966"
        });
    }
});