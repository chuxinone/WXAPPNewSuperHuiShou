function e(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}

var r = e(require("../../../util/utils")), t = e(require("../../../model/order")), n = (e(require("../../../model/user")), 
e(require("../../../model/partner"))), o = void 0, a = getApp();

Page({
    data: {
        orderInfo: {},
        curTime: "",
        isIphoneX: a.globalData.isIphoneX,
        location: "",
        setting: {}
    },
    onLoad: function(e) {
        o = this;
        var t = r.default.formatDate(), n = {};
        e.orderInfo && (n = JSON.parse(e.orderInfo)), o.setData({
            orderInfo: n,
            curTime: t.Y + "-" + t.M + "-" + t.D + " " + t.h + ":" + t.m + ":" + t.s
        }), console.log(n), o.asyncSfOrder(n), o.asyncFcOrder(n), o.asyncPartnerOrder(n);
    },
    asyncSfOrder: function(e) {
        "shunfeng" == e.way && t.default.takeSfOrder(e).then(function(r) {
            e.trackNum = r.mailno, o.setData({
                orderInfo: e
            });
        }, function(e) {
            console.log(e);
        });
    },
    asyncFcOrder: function(e) {
        "fengchao" == e.way && t.default.takeFcOrder(e).then(function(r) {
            console.log(r), e.sendCode = r.sendCode, e.sendId = r.sendId, o.setData({
                orderInfo: e
            });
        }, function(e) {
            console.log(e);
        });
    },
    asyncPartnerOrder: function(e) {
        var r = n.default.getPartnerApi(), t = n.default.store.extraData;
        if (!r) return !1;
        r.trade({
            sid: t.sid,
            uid: t.uid,
            pid: t.pid,
            orderNum: e.ordernum,
            tel: e.tel,
            productName: e.productName,
            productId: parseInt(e.itemid),
            price: e.price
        }).then(function(e) {
            console.log(e);
        }, function(e) {
            console.log(e);
        });
    },
    switchPage: function(e) {
        var r = e.currentTarget.dataset;
        wx.navigateTo({
            url: r.url
        });
    },
    onShow: function() {
        wx.hideShareMenu();
        var e = wx.getStorageSync("setting");
        this.setData({
            setting: e
        });
    }
});