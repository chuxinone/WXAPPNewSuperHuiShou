function e(e) {
    return e && e.__esModule ? e : {
        default: e
    };
}

var t = e(require("../../../util/utils")), a = e(require("../../../model/product")), n = void 0, c = getApp();

Page({
    data: {
        base: [],
        func: [],
        selects: {},
        funcMap: {},
        product: {},
        pInfo: {},
        baseStore: [],
        cdn: c.globalData.cdn,
        baseSelect: [],
        funcSelect: [],
        fullChoice: 0,
        len: 0,
        toView: "view0",
        alertInfo: {
            title: "",
            desc: "",
            pictureUrl: ""
        },
        canClick: !0,
        isIphoneX: c.globalData.isIphoneX
    },
    onLoad: function(e) {
        var c = this;
        t.default.setWhiteNavBar(), n = this, a.default.productInfo({
            productId: e.productId
        }).then(function(e) {
            var t = [], a = [], i = void 0;
            e.itemList.forEach(function(e) {
                1 === (i = parseInt(e.conftype)) || 2 === i ? t.push(e) : 3 === i && (a = e);
            }), c.setValues(t, "base"), console.log("base", t), a.question && c.setValues(a, "func"), 
            n.setData({
                base: t,
                func: a,
                pInfo: {
                    picUrl: e.picurl,
                    classId: e.classid,
                    productId: e.itemid,
                    maxPrice: e.maxprice,
                    productName: e.productname,
                    standardPrice: e.standardprice
                },
                alertTip: !1
            }), wx.setNavigationBarTitle({
                title: n.data.pInfo.productName + " 估价"
            });
        });
    },
    onShow: function() {
        wx.hideShareMenu();
    },
    funcHandler: function(t) {
        var a = [], n = e.id, c = void 0, i = void 0, o = void 0, d = void 0, r = e.name;
        return e.question.map(function(e) {
            c = e.id, i = e.name, o = e.picture, d = e.name, a.push({
                kid: n,
                did: void 0,
                oid: c,
                desc: i,
                picture: o,
                otext: d,
                dtext: void 0,
                ptext: r
            });
        }), a;
    },
    alertTipTapHandler: function() {
        n.setData({
            alertTip: !1
        });
    },
    baseTapHandler: function(e) {
        for (var t = e.currentTarget.dataset, a = n.data.baseSelect, c = 0; c < a.length; c++) a[c].pid === t.pid && (a[c].cid = t.cid, 
        a[c].ctext = t.cname);
        n.setData({
            baseSelect: a
        }), n.onSelectsChanged(!0);
    },
    setValues: function(e, t) {
        var a = [];
        "base" === t && (e.map(function(e) {
            var t = {};
            t.pid = e.id, t.ptext = e.name, t.cid = "", t.ctext = "", a.push(t);
        }), n.setData({
            baseSelect: a
        })), "func" === t && (console.log("eeeee", e), e.question.map(function(e) {
            var t = {};
            t.cid = "", t.ctext = "", a.push(t);
        }), n.setData({
            funcSelect: a
        }));
    },
    funcTapHandler: function(e) {
        var t = e.currentTarget.dataset, a = n.data.funcSelect;
        console.log(t);
        for (var c = 0; c < a.length; c++) c == t.index && (a[c].cid == t.cid ? (a[c].cid = "", 
        a[c].ctext = "") : (a[c].cid = t.cid, a[c].ctext = t.ctext));
        n.setData({
            funcSelect: a
        }), n.onSelectsChanged();
    },
    onSelectsChanged: function(e) {
        for (var t = 0, a = n.data.base, c = n.data.baseSelect, i = 0; i < c.length; i++) c[i].cid && t++;
        if (e) {
            var o = t === a.length ? "view" + (t + 1) : "view" + t;
            if (t === a.length) {
                var d = n.data.fullChoice + 1;
                n.setData({
                    fullChoice: d
                });
            }
            n.data.fullChoice > 1 ? n.setData({
                len: t
            }) : n.setData({
                len: t,
                toView: o
            });
        } else n.setData({
            len: t
        });
    },
    onSubmit: function() {
        n.setData({
            canClick: !1
        }), console.log(n.data.funcSelect);
        for (var e = n.data.funcSelect, t = [], c = [], i = 0, o = 0; i < e.length; i++) "" != e[i].cid && (t[o] = e[i].cid, 
        c[o] = e[i].ctext, o++);
        var d = n.data.pInfo, r = n.data.baseSelect, u = r.map(function(e) {
            return e.cid;
        }), l = r.map(function(e) {
            return e.ctext;
        }), s = "", f = "";
        t.length > 0 ? (s = u.join("-") + "-" + t.join("|"), f = l.join("-") + "-" + c.join("|")) : (s = u.join("-"), 
        f = l.join("-")), a.default.evaluate({
            productId: n.data.pInfo.productId,
            selects: s
        }).then(function(e) {
            n.setData({
                canClick: !0
            }), wx.navigateTo({
                url: "../price/index?price=" + e.quotation + "&ids=" + s + "&desc=" + f + "&productId=" + d.productId + "&productName=" + d.productName + "&classId=" + d.classId
            });
        }, function(e) {
            console.log(e), n.setData({
                canClick: !0
            });
        });
    },
    showPictures: function(e) {
        var t = e.currentTarget.dataset, a = n.data.base[t.iindex], c = a.question[t.qindex], i = "" + a.name, o = c.picture[0].picturename, d = c.name;
        n.setData({
            alertInfo: {
                desc: d,
                title: i,
                pictureUrl: o
            }
        });
    },
    showFunPictures: function(e) {
        var t = e.currentTarget.dataset;
        n.setData({
            alertInfo: {
                desc: t.desc,
                title: t.title,
                pictureUrl: t.img
            }
        });
    },
    closeAlert: function() {
        n.setData({
            alertInfo: {}
        });
    }
});