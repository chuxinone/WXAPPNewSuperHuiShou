function o(o) {
    return o && o.__esModule ? o : {
        default: o
    };
}

o(require("../../../util/utils"));

var n = o(require("../../../model/coupon")), e = o(require("../../../model/user")), t = void 0;

getApp(), Page({
    data: {
        ruleShow: !1,
        couponShow: !1,
        userInfo: {}
    },
    onLoad: function(o) {
        t = this;
    },
    ruleSee: function() {
        t.setData({
            ruleShow: !0
        });
    },
    couponGet: function(o) {
        o.currentTarget.dataset, t.data.userInfo.tel ? n.default.gain({
            userId: t.data.userInfo.old_uid,
            id: 2
        }).then(function(o) {
            t.setData({
                couponShow: !0
            });
        }, function(o) {
            wx.showModal({
                title: "提示",
                content: o,
                showCancel: !1
            });
        }) : wx.showModal({
            title: "提示",
            content: "请先绑定手机号",
            showCancel: !1,
            success: function(o) {
                wx.navigateTo({
                    url: "../bind/index"
                });
            }
        });
    },
    closeCoupon: function() {
        t.setData({
            couponShow: !1
        });
    },
    closeRule: function() {
        t.setData({
            ruleShow: !1
        });
    },
    onReady: function() {},
    onShow: function() {
        var o = e.default.getUserInfo();
        t.setData({
            userInfo: o
        });
    },
    onHide: function() {},
    onUnload: function() {},
    onPullDownRefresh: function() {},
    onReachBottom: function() {},
    onShareAppMessage: function(o) {
        return "button" === o.from && console.log(o.target), {
            title: "专业的手机数码估价回收平台",
            path: "page/pages/couponget/index",
            success: function(o) {},
            fail: function(o) {}
        };
    }
});