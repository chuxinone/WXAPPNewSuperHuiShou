var n = function(n) {
    return n && n.__esModule ? n : {
        default: n
    };
}(require("../../../model/user"));

Page({
    data: {},
    onLoad: function(n) {},
    onReady: function() {},
    onShow: function() {
        n.default.getWxUnionId().then(function(o) {
            n.default.getWxCode().then(function(e) {
                n.default.decryptWxInfo({
                    code: e,
                    iv: o.iv,
                    encryptedData: o.encryptedData
                }).then(function(n) {
                    var o = n.data.data;
                    console.log("decryptWxInfo", o);
                });
            });
        });
    },
    onHide: function() {},
    onUnload: function() {},
    onPullDownRefresh: function() {},
    onReachBottom: function() {},
    onShareAppMessage: function() {}
});