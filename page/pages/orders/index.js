function t(t) {
    return t && t.__esModule ? t : {
        default: t
    };
}

var e = t(require("../../../model/order")), a = t(require("../../../model/user")), o = void 0, r = getApp();

Page({
    data: {
        orderList: [],
        cdn: r.globalData.cdn,
        num: 0,
        total: 0,
        userInfo: {},
        hasMore: !0,
        isShowLoadText: !0,
        isShowNoMoreText: !1,
        setting: {}
    },
    onLoad: function() {
        o = this, wx.setNavigationBarTitle({
            title: "订单中心"
        });
    },
    onShow: function() {
        var t = wx.getStorageSync("setting");
        this.setData({
            setting: t
        }), wx.hideShareMenu();
        var e = a.default.getUserInfo();
        o.setData({
            isShowLoadText: !0,
            userInfo: e
        }), o.loadMore();
    },
    loadMore: function() {
        o.data.hasMore || (o.setData({
            isShowNoMoreText: !0
        }), setTimeout(function() {
            o.setData({
                isShowNoMoreText: !1
            });
        }, 1e3));
        var t = o.data.userInfo, a = o.data.num;
        e.default.orders({
            uid: t.uid,
            openid: t.openid,
            pageIndex: a
        }).then(function(t) {
            var e = o.data.orderList, r = t.total, n = r > a;
            t.order_list.length && (e = e.concat(t.order_list), o.setData({
                total: r,
                num: e.length,
                hasMore: n,
                orderList: e,
                isShowLoadText: !1
            }));
        }, function(t) {
            console.log(t);
        });
    },
    refresh: function() {
        o.setData({
            num: 0,
            orderList: []
        }), o.loadMore();
    },
    handleCancel: function(t) {
        t.currentTarget.dataset;
        var a = o.data.orderList;
        wx.showModal({
            title: "提示",
            content: "您真的要取消吗？",
            success: function(r) {
                if (r.confirm) {
                    var n = o.data.userInfo, i = t.currentTarget.dataset;
                    e.default.cancelOrder({
                        orderid: i.orderid,
                        uid: n.uid,
                        openid: n.openid
                    }).then(function(t) {
                        a[i.index].can_cancel = 0, a[i.index].order_status_name = "已取消", o.setData({
                            orderList: a
                        });
                    }, function(t) {
                        wx.showModal({
                            title: "提示",
                            content: "取消订单失败，请联系客服",
                            showCancel: !1
                        });
                    });
                }
            }
        });
    },
    switchPage: function(t) {
        var e = t.currentTarget.dataset;
        console.log(e.url), wx.navigateTo({
            url: e.url
        });
    },
    switchToTab: function(t) {
        var e = t.currentTarget.dataset;
        wx.switchTab({
            url: e.url
        });
    }
});