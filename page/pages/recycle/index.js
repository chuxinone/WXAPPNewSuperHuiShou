var n = getApp();

Page({
    data: {
        isIphoneX: n.globalData.isIphoneX
    },
    onLoad: function(n) {},
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    onPullDownRefresh: function() {},
    onReachBottom: function() {},
    onShareAppMessage: function(n) {
        return "button" === n.from && console.log(n.target), {
            title: "专业的手机数码估价回收平台",
            path: "page/pages/recycle/index",
            success: function(n) {},
            fail: function(n) {}
        };
    }
});