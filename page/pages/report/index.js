Page({
    data: {
        reportUrl: ""
    },
    onLoad: function(t) {
        this.setData({
            reportUrl: "https://mobile.huishoubao.com/mobile/qtreport_" + t.reportId + ".html"
        });
    },
    onShow: function() {
        wx.hideShareMenu();
    }
});