var t = function(t) {
    return t && t.__esModule ? t : {
        default: t
    };
}(require("../../../model/product")), a = void 0, d = getApp();

Page({
    data: {
        cateList: [{classname:'手机',classid:1},{classname:'平板',classid:2},{classname:'笔记本',classid:3}],
        brandList: [{brandid:12,brandname:'iphone4'},{brandid:13,brandname:'iphone4'},{brandid:14,brandname:'iphone4'}],
        productList: [{productid:123,productname:'iphone4'},{productid:124,productname:'iphone4'},{productid:125,productname:'iphone4'}],
        cid: 1,
        bid: -1,
        num: 0,
        hasMore: !0,
        brandScrollTop: 0,
        productScrollTop: 0,
        left: 0,
        iconSearch: "../../../img/icon-search.svg"
    },
    onLoad: function() {
        a = this, t.default.category().then(function(t) {
            var n = t.classlist, e = d.globalData.cid;
            d.globalData.bid, a.setData({
                cateList: t.classlist
            });
            for (var i = 0, o = 0; o < n.length; o++) if (n[o].classid == e) {
                i = o;
                break;
            }
            a.setData({
                left: parseInt(i) / n.length * 100 + "%"
            });
        });
    },
    onShow: function() {
        for (var t = d.globalData.cid, n = d.globalData.bid, e = a.data.cateList, i = 0, o = 0; o < e.length; o++) if (e[o].classid == t) {
            i = o;
            break;
        }
        a.setData({
            cid: t,
            bid: n,
            left: parseInt(i) / e.length * 100 + "%"
        }), a.onCidChanged(n);
    },
    cateTapHandler: function(t) {
        var d = t.currentTarget.dataset, n = a.data.cateList;
        a.setData({
            cid: d.cid,
            left: d.index / n.length * 100 + "%"
        }), a.onCidChanged();
    },
    onCidChanged: function(d) {
        t.default.brands({
            cid: a.data.cid
        }).then(function(n) {
            a.setData({
                brandList: n.brandlist,
                bid: n.brandlist[0].brandid,
                brandScrollTop: 0
            }), void 0 !== d && a.setData({
                bid: d
            }), t.default.products({
                cid: a.data.cid,
                bid: a.data.bid,
                num: 1
            }).then(function(t) {
                a.setData({
                    productList: t.productlist,
                    hasMore: 1 < t.pagenum,
                    num: 1,
                    productScrollTop: 0
                });
            });
        });
    },
    brandTapHandler: function(t) {
        var d = t.currentTarget.dataset;
        a.setData({
            bid: d.bid
        }), a.onBrandIdChanged();
    },
    onBrandIdChanged: function() {
        t.default.products({
            cid: a.data.cid,
            bid: a.data.bid,
            num: 1
        }).then(function(t) {
            a.setData({
                productList: t.productlist,
                num: 1,
                hasMore: 1 < t.pagenum,
                productScrollTop: 0
            });
        });
    },
    loadMoreProduct: function() {
        if (console.log(1), a.data.hasMore) {
            var d = ++a.data.num;
            t.default.products({
                cid: a.data.cid,
                bid: a.data.bid,
                num: d
            }).then(function(t) {
                var n = a.data.productList;
                a.setData({
                    productList: n.concat(t.productlist),
                    hasMore: d < t.pagenum,
                    num: d
                });
            });
        }
    },
    onShareAppMessage: function(t) {
        return "button" === t.from && console.log(t.target), {
            title: "专业的手机数码估价回收平台",
            path: "page/pages/products/index",
            success: function(t) {},
            fail: function(t) {}
        };
    }
});