function t(t) {
    return t && t.__esModule ? t : {
        default: t
    };
}

var e = t(require("../../../util/utils")), n = t(require("../../../model/coupon")), o = t(require("../../../model/user")), a = void 0;

getApp(), Page({
    data: {
        menuIndex: 0,
        left: 0,
        menuList: [ {
            text: "未使用",
            length: 2
        }, {
            text: "已使用",
            length: 2
        }, {
            text: "已过期",
            length: 2
        } ],
        token: ""
    },
    onShow: function() {
        wx.hideShareMenu(), a = this, wx.setNavigationBarTitle({
            title: "我的优惠券"
        }), wx.setNavigationBarColor({
            frontColor: "#000000",
            backgroundColor: "#ffffff"
        });
        var t = o.default.getUserInfo();
        a.setData({
            userInfo: t
        }), a.loadCoupons();
    },
    loadCoupons: function() {
        var t = a.data.userInfo, o = a.data.menuList;
        Promise.all([ n.default.page({
            userId: t.old_uid,
            status: 1
        }), n.default.page({
            userId: t.old_uid,
            status: 2
        }), n.default.page({
            userId: t.old_uid,
            status: 3
        }) ]).then(function(t) {
            console.log(t), t.forEach(function(t, n) {
                (t = t.couponUseLogList) && (t = t.map(function(t) {
                    return t.value = parseInt(t.couponValue) / 100, t.validEndTime = t.validEndTime.substring(0, 10), 
                    t;
                }), e.default.sortByKey(t, "value")), o[n].data = t;
            }), console.log(o), a.setData({
                menuList: o
            });
        }, function(t) {
            console.log(t);
        });
    },
    setLeft: function() {
        var t = a.data.menuIndex / 3 * 100;
        a.setData({
            left: t
        });
    },
    handleMenu: function(t) {
        var e = t.currentTarget.dataset;
        console.log(e), a.setData({
            menuIndex: e.index
        });
    },
    handleToken: function(t) {
        a.setData({
            token: t.detail.value
        });
    },
    addCoupon: function() {
        n.default.gain({
            id: a.data.token,
            userId: a.data.userInfo.old_uid
        }).then(function(t) {
            wx.showToast({
                title: "添加成功"
            }), a.loadCoupons(), a.setData({
                token: ""
            });
        }, function(t) {
            wx.showModal({
                title: "提示",
                content: t,
                showCancel: !1
            }), a.setData({
                token: ""
            });
        });
    }
});