Page({
    data: {
        setting: {}
    },
    onShow: function() {
        wx.hideShareMenu();
        var t = wx.getStorageSync("setting");
        this.setData({
            setting: t
        });
    }
});