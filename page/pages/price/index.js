function t(t) {
    return t && t.__esModule ? t : {
        default: t
    };
}

t(require("../../../model/product")), t(require("../../../model/trade"));

var e = t(require("../../../model/order")), a = t(require("../../../model/user")), s = t(require("../../../model/coupon")), n = t(require("../../../util/utils")), i = void 0, d = getApp();

Page({
    data: {
        firstTap: 0,
        desc: [],
        price: 0,
        months: [],
        values: [],
        selects: [],
        productName: "",
        productId: "",
        classId: "",
        brandId: "",
        isOpen: !1,
        modelStatus: !1,
        animationData: {},
        animationpriceData: {},
        animationShowHeight: 0,
        submitNow: !1,
        ways: [ {
            name: "快递回收(包邮)",
            way: "post",
            iconBlack: "../../../img/price/kuaidi.svg",
            iconWhite: "../../../img/price/kuaidi-f.svg"
        }, {
            name: "上门回收",
            way: "visit",
            iconBlack: "../../../img/price/shangmen.svg",
            iconWhite: "../../../img/price/shangmen-f.svg"
        } ],
        way: "post",
        postway: "self",
        sf: {
            addr: {
                data: [],
                indexs: [ 0, 0, 0 ],
                selects: [ [], [], [] ],
                value: ""
            },
            date: {
                data: [],
                indexs: [ 0, 0 ],
                selects: [ [], [] ],
                value: ""
            },
            street: ""
        },
        hsb: {
            addr: {
                data: [],
                indexs: [ 0, 0 ],
                selects: [ [], [] ],
                value: ""
            },
            date: {
                data: [],
                indexs: [ 0, 0 ],
                selects: [ [], [] ],
                value: ""
            },
            street: ""
        },
        fc: {
            cardid: "",
            addr: {
                data: [],
                indexs: [ 0, 0, 0 ],
                selects: [ [], [], [] ],
                value: ""
            },
            street: ""
        },
        nickname: "",
        tel: "",
        coupon: {},
        couponList: [],
        choicedIndex: -1,
        userInfo: {},
        telFocus: !1,
        nicknameFocus: !1,
        options: {},
        initCityData: {
            province: "",
            city: "",
            district: ""
        },
        location: "",
        isIphoneX: d.globalData.isIphoneX,
        desShow: !1,
        devicePer: 2,
        setting: {}
    },
    onLoad: function(t) {
        i = this, wx.getSetting({
            success: function(t) {
                console.log(t), !1 === t.authSetting["scope.userLocation"] && wx.authorize({
                    scope: "scope.userLocation",
                    success: function(t) {
                        console.log(t);
                    },
                    fail: function(t) {
                        wx.showModal({
                            title: "温馨提醒",
                            content: "需要获取您的地理位置才能使用小程序",
                            cancelText: "不使用",
                            confirmText: "获取位置",
                            success: function(t) {
                                t.confirm && wx.openSetting({
                                    success: function(t) {
                                        t.authSetting = {
                                            "scope.userLocation": !0
                                        };
                                    },
                                    complete: function(t) {}
                                });
                            }
                        });
                    },
                    complete: function(t) {
                        console.log(t);
                    }
                });
            }
        }), console.log(t);
        var e = parseInt(t.price) / 100, a = t.productId;
        i.setData({
            price: e,
            productId: a,
            selects: t.ids,
            desc: t.desc.split("-"),
            productName: t.productName,
            classId: t.classId
        }), wx.getSystemInfo({
            success: function(t) {
                var e = 750 / t.screenWidth;
                i.setData({
                    devicePer: e
                });
            }
        }), i.setData({
            options: t
        });
    },
    switchPage: function(t) {
        var e = t.currentTarget.dataset;
        wx.navigateTo({
            url: e.url + "?location=" + i.data.location
        });
    },
    toggleOptions: function() {
        i.setData({
            isOpen: !i.data.isOpen
        });
    },
    goBack: function() {
        wx.navigateBack();
    },
    loadCoupons: function(t) {
        var e = -1, n = i.data.couponList, o = a.default.getUserInfo();
        s.default.canUse({
            userId: o.old_uid,
            proId: i.data.productId,
            price: 100 * i.data.price,
            recycleType: t
        }).then(function(t) {
            (n = t.couponsList).length && (n.map(function(t, a) {
                return t.value = parseInt(t.couponsValue) / 100, t.minQuation = parseInt(t.minQuation) / 100, 
                t.deadline = t.couponsValidity.substring(0, 10), 1 == t.isCanUse && 0 == a && (e = 0), 
                t;
            }), d.globalData.couponsList = n, i.setData({
                coupon: n[e] || {},
                choicedIndex: e,
                couponList: n
            }));
        });
    },
    onShow: function() {
        var t = wx.getStorageSync("setting");
        this.setData({
            setting: t
        }), wx.hideShareMenu();
        var e = a.default.getUserInfo();
        i.data.tel, e && e.tel ? i.setData({
            userInfo: e,
            tel: e.tel
        }) : i.setData({
            userInfo: e
        }), e.tel && !i.data.couponList.length && i.loadCoupons();
        var s = d.globalData.couponsList;
        i.setData({
            coupon: s[i.data.choicedIndex] || {},
            couponList: s
        });
        var n = wx.createAnimation({
            duration: 500,
            timingFunction: "ease"
        }), o = wx.createAnimation({
            duration: 500,
            timingFunction: "ease"
        });
        i.animation = o, i.animationprice = n, wx.getSystemInfo({
            success: function(t) {
                i.animationShowHeight = t.windowHeight;
            }
        });
    },
    submitFull: function(t) {
        switch (i.data.way) {
          case "visit":
            console.log("o.data", i.data);
            var e = i.data.nickname && i.data.tel && i.data.hsb.street;
            i.setData({
                submitNow: e
            });
            break;

          case "post":
            if ("sf" == i.data.postway) {
                var a = i.data.nickname && i.data.tel && i.data.sf.street;
                i.setData({
                    submitNow: a
                });
            } else {
                var s = i.data.nickname && i.data.tel;
                i.setData({
                    submitNow: s
                });
            }
            break;

          case "fengchao":
            var n = i.data.nickname && i.data.tel && i.data.fc.cardid && i.data.fc.street;
            i.setData({
                submitNow: n
            });
        }
    },
    showPriceDesc: function() {
        1 != i.data.desShow ? i.animationprice.height(90).step() : i.animationprice.height(0).step(), 
        i.setData({
            animationpriceData: i.animationprice.export(),
            desShow: !i.data.desShow
        });
    },
    hidePriceDesc: function() {
        i.animationprice.height(0).step(), i.setData({
            animationpriceData: i.animationprice.export(),
            desShow: !1
        });
    },
    animationShow: function() {
        i.animation.translateY(-i.animationShowHeight).step(), i.setData({
            animationData: i.animation.export(),
            modalStatus: !0
        });
    },
    animationHide: function() {
        i.animation.translateY(i.animationShowHeight).step(), i.setData({
            animationData: i.animation.export(),
            modalStatus: !1
        });
    },
    sfAddrFormat: function(t) {
        console.log(t);
        for (var e = [], a = 0; a < 3; a++) t.addr.selects[a].length && e.push(t.addr.selects[a][t.addr.indexs[a]].name);
        return e.join(" ");
    },
    provinceToIndex: function(t, e) {
        for (var a = 0; a < e.length; a++) if (e[a].name == t) return a;
        return 0;
    },
    cityToIndex: function(t, e) {
        for (var a = 0; a < e.length; a++) for (var s = 0; s < e[a].sub.length; s++) if (e[a].sub[s].name == t) return s;
        return 0;
    },
    districtToIndex: function(t, e) {
        for (var a = 0; a < e.length; a++) for (var s = 0; s < e[a].sub.length; s++) if (e[a].sub[s].sub) for (var n = 0; n < e[a].sub[s].sub.length; n++) if (e[a].sub[s].sub[n].name == t) return n;
        return 0;
    },
    hsbAddrFormat: function(t) {
        for (var e = [], a = 0; a < 2; a++) t.addr.selects[a].length && e.push(t.addr.selects[a][t.addr.indexs[a]].name);
        return e.join(" ");
    },
    handleSfAddr: function(t) {
        var e = this.data.sf;
        e.addr.indexs = t.detail.value, this.setData({
            sf: e
        });
    },
    handleFcAddr: function(t) {
        var e = this.data.fc;
        e.addr.indexs = t.detail.value, this.setData({
            fc: e
        });
    },
    handleSfAddrCol: function(t) {
        var e = i.data.sf, a = t.detail.value, s = t.detail.column;
        switch (s) {
          case 0:
            e.addr.selects[1] = e.addr.data[a].sub, e.addr.selects[1][0].sub ? e.addr.selects[2] = e.addr.selects[1][0].sub : e.addr.selects[2] = [], 
            e.addr.indexs[s + 1] = 0, e.addr.indexs[s + 2] = 0;
            break;

          case 1:
            e.addr.selects[s][a].sub ? e.addr.selects[2] = e.addr.selects[s][a].sub : e.addr.selects[2] = [], 
            e.addr.indexs[s + 1] = 0;
        }
        e.addr.indexs[s] = a, e.addr.value = i.sfAddrFormat(e), this.setData({
            sf: e
        });
    },
    handleFcAddrCol: function(t) {
        var e = i.data.fc, a = t.detail.value, s = t.detail.column;
        switch (s) {
          case 0:
            e.addr.selects[1] = e.addr.data[a].sub, e.addr.selects[1][0].sub ? e.addr.selects[2] = e.addr.selects[1][0].sub : e.addr.selects[2] = [], 
            e.addr.indexs[s + 1] = 0, e.addr.indexs[s + 2] = 0;
            break;

          case 1:
            e.addr.selects[s][a].sub ? e.addr.selects[2] = e.addr.selects[s][a].sub : e.addr.selects[2] = [], 
            e.addr.indexs[s + 1] = 0;
        }
        e.addr.indexs[s] = a, e.addr.value = i.sfAddrFormat(e), this.setData({
            fc: e
        });
    },
    handleHsbAddrCol: function(t) {
        var e = i.data.hsb, a = t.detail.value, s = t.detail.column;
        switch (s) {
          case 0:
            e.addr.selects[1] = e.addr.data[a].sub, e.addr.indexs[s] = a, e.addr.indexs[s + 1] = 0;
            break;

          case 1:
            e.addr.indexs[1] = a;
        }
        e.addr.value = i.hsbAddrFormat(e), this.setData({
            hsb: e
        });
    },
    handleSfDateCol: function(t) {
        var e = i.data.sf, a = t.detail.value, s = t.detail.column;
        0 == s && (e.date.selects[1] = e.date.data.time[a]), e.date.indexs[s] = a, e.date.value = i.formatSfDate(e), 
        this.setData({
            sf: e
        });
    },
    handleHsbDateCol: function(t) {
        var e = i.data.hsb, a = t.detail.value, s = t.detail.column;
        0 == s && (e.date.selects[1] = e.date.data.time[a]), e.date.indexs[s] = a, e.date.value = this.formatHsbDate(e), 
        this.setData({
            hsb: e
        });
    },
    formatSfDate: function(t) {
        for (var e = [], a = 0; a < 2; a++) t.date.selects[a].length && e.push(t.date.selects[a][t.date.indexs[a]]);
        return e.join(" ");
    },
    formatHsbDate: function(t) {
        for (var e = [], a = 0; a < 2; a++) t.date.selects[a].length && e.push(t.date.selects[a][t.date.indexs[a]]);
        return e.join(" ");
    },
    switchWay: function(t) {
        var e = t.currentTarget.dataset, a = i.data.options;
        if (e.way !== i.data.way) {
            if ("visit" === e.way && parseInt(a.price) < 1e4) return void i.showModel("未满100元不支持上门回收");
            i.setData({
                way: e.way
            }), i.submitFull();
        }
    },
    switchPostWay: function(t) {
        var e = t.currentTarget.dataset;
        i.data.options, i.loadCoupons(), i.setData({
            postway: e.postway
        }), i.submitFull();
    },
    validParam: function(t) {
        return t && t.length > 0;
    },
    showModel: function(t) {
        wx.showModal({
            title: "提示",
            content: t,
            showCancel: !1
        });
    },
    takeHsbVisitOrder: function(t) {
        var a = i.data.hsb, s = a.street, n = a.addr.value + s, d = a.date.value;
        if (!i.validParam(s)) return i.showModel("请填写详细地址"), !1;
        t.ordertype = "visit", t.address = n, t.displayVisitTime = d, t.way = "shangmen", 
        e.default.take(t).then(function(e) {
            Object.assign(t, e), wx.reLaunch({
                url: "../success/index?orderInfo=" + JSON.stringify(t)
            });
        }, function(t) {
            i.showModel(t), i.setData({
                firstTap: 1
            }), console.log(t);
        });
    },
    takeSfOrder: function(t) {
        var a = this, s = i.data.sf, n = s.street, d = s.addr.value, o = s.date.value;
        if (!i.validParam(n)) return i.showModel("请填写详细地址");
        if (!i.validParam(d)) return i.showModel("请选择城市");
        if (!i.validParam(o)) return i.showModel("请选择预约时间");
        console.log(o);
        var r = new Date((o.substring(0, 16) + ":00").replace(/-/g, "/")).getTime() / 1e3, c = d.split(" ");
        t.ordertype = "post", e.default.take(t).then(function(e) {
            t.ordernum = e.ordernum, t.orderid = e.orderid, t.sendtime = r, t.province = c[0] || "", 
            t.city = c[1] || "", t.county = c[2] || "", t.addr = d + " " + n, t.displayVisitTime = o, 
            t.way = "shunfeng", wx.reLaunch({
                url: "../success/index?orderInfo=" + JSON.stringify(t)
            });
        }, function(t) {
            a.showModel(t), a.setData({
                firstTap: 1
            });
        });
    },
    takeSelfOrder: function(t) {
        var a = this;
        t.ordertype = "post", e.default.take(t).then(function(e) {
            console.log("price:", e), t.postway = "self", t.ordernum = e.ordernum, t.orderid = e.orderid, 
            t.way = "self", wx.reLaunch({
                url: "../success/index?orderInfo=" + JSON.stringify(t)
            });
        }, function(t) {
            a.showModel(t), a.setData({
                firstTap: 1
            });
        });
    },
    takeFcOrder: function(t) {
        var a = this, s = i.data.fc, d = s.street, o = s.cardid, r = s.addr.value;
        if (!n.default.isCardid(o)) return i.showModel("请输入正确的身份证号码");
        if (!i.validParam(d)) return i.showModel("请填写详细地址");
        if (!i.validParam(r)) return i.showModel("请选择城市");
        var c = r.split(" ");
        t.ordertype = "post", console.log(t), e.default.take(t).then(function(e) {
            t.ordernum = e.ordernum, t.orderid = e.orderid, t.cardid = o, t.province = c[0] || "", 
            t.city = c[1] || "", t.county = c[2] || "", t.addr = r + " " + d, t.way = "fengchao", 
            wx.reLaunch({
                url: "../success/index?orderInfo=" + JSON.stringify(t)
            });
        }, function(t) {
            a.showModel(t), a.setData({
                firstTap: 1
            });
        });
    },
    submitOrder: function() {
        var t = i.data.firstTap;
        if (!(++t > 1)) {
            var e = i.data.tel, s = i.data.nickname;
            if (s.length < 2) return wx.showModal({
                title: "提示",
                content: "请输入正确的联系人姓名",
                showCancel: !1
            });
            if (!n.default.isMobile(e)) return wx.showModal({
                title: "提示",
                content: "手机号码格式不正确",
                showCancel: !1
            });
            var d = i.data.options, o = a.default.getUserInfo(), r = i.data.coupon, c = {
                selects: d.ids,
                itemid: d.productId,
                uid: o.uid,
                openid: o.openid,
                nickname: s,
                tel: e
            };
            c.productName = d.productName, c.price = d.price, c.couponId = r && r.couponsId;
            var u = i.data.way, l = i.data.postway;
            "post" === u && ("sf" === l && i.takeSfOrder(c), "self" === l && i.takeSelfOrder(c)), 
            "visit" === u && i.takeHsbVisitOrder(c), "fengchao" === u && i.takeFcOrder(c), i.setData({
                firstTap: t
            });
        }
    },
    showCoupDesc: function() {
        wx.showModal({
            title: "现金券说明",
            content: '系统自动使用您的可以使用的最大面值现金券\r\n关注"回收宝"公众号,送千万大礼',
            showCancel: !1
        });
    },
    showFcDesc: function() {
        wx.showModal({
            title: "丰巢回收说明",
            content: "您可以根据我们的推荐选择任意快递柜，将手机放入格口，无需等待，快递小哥自动取件邮寄到目的地",
            showCancel: !1
        });
    },
    handleName: function(t) {
        this.setData({
            nickname: t.detail.value
        });
    },
    handleTel: function(t) {
        this.setData({
            tel: t.detail.value
        });
    },
    onTelFocus: function(t) {
        i.setData({
            telFocus: !0
        });
    },
    onTelBlur: function(t) {
        i.setData({
            telFocus: !1
        }), i.submitFull();
    },
    onSfStreetBlur: function(t) {
        var e = i.data.sf;
        e.street = t.detail.value, this.setData({
            sf: e
        }), i.submitFull();
    },
    onHsbStreetBlur: function(t) {
        var e = i.data.hsb;
        e.street = t.detail.value, this.setData({
            hsb: e
        }), i.submitFull();
    },
    onFcCardidBlur: function(t) {
        var e = i.data.fc;
        e.cardid = t.detail.value, this.setData({
            fc: e
        }), i.submitFull();
    },
    onFcStreetBlur: function(t) {
        var e = i.data.fc;
        e.street = t.detail.value, this.setData({
            fc: e
        }), i.submitFull();
    },
    onNicknameBlur: function(t) {
        i.setData({
            nicknameFocus: !1
        }), i.submitFull();
    },
    onNicknameFocus: function(t) {
        console.log(t), i.setData({
            nicknameFocus: !0
        });
    }
});