Object.defineProperty(exports, "__esModule", {
    value: !0
});

var e = require("../siteinfo.js"), r = e.siteroot + "?i=" + e.uniacid + "&token=" + e.token + "&version=" + e.version + "&c=entry&m=ybz_huishou&a=wxapp&do=";

exports.gdKey = "19932279131320c2dbbbd31c7a8a05e7", exports.url = {
    getWxOpenId: r + "getWxOpenId",
    saveUserInfo: r + "saveUserInfo",
    getSetting: r + "getSetting",
    bindTelLogin: r + "bindTelLogin",
    getCode: r + "getCode",
    orders: r + "getOrderList",
    order: r + "getOrderDetails",
    cancelOrder: r + "cancelOrder",
    hsbCity: r + "hsbCity/",
    support_visit_time: r + "support_visit_time/",
    takeOrder: r + "takeOrder",
    takeSfOrder: r + "takeSfOrder",
    takeFcOrder: r + "takeFcOrder",
    categories: r + "categories",
    brands: r + "brands",
    products: r + "products",
    productInfo: r + "productInfo",
    search: r + "search",
    price: r + "price",
    priceHistory: r + "priceHistory/",
    sfCity: r + "getSfCity/",
    modelMap: "https://app.huanjixia.com/api/appstore",
    getBoxInfo: r + "api"
};